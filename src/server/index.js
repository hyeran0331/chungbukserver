const express = require('express');
const cors = require('cors');
const session = require('express-session');
const bodyParser = require('body-parser');
// const fileUpload = require('express-fileupload');
const nodemailer = require('nodemailer');
const path = require('path');
const os = require('os');
const app = express();
const port = process.env.PORT || 8083;
const initDatabase = require('./db/orm/database_init');
const transferLogDao = require('./dao/admin/transfer_log_dao')
const transferLog = require('./common/transfer_log')

const multer = require('multer');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

initDatabase.initDatabase();

app.use(cors({
  credentials: true,
  origin: function(origin, callback) {
    return callback(null, true);
  }
}));

app.use(bodyParser.json({
  limit: 5242880
}));
app.use(bodyParser.urlencoded({
  limit: 5242880,
  extended: true,
  parameterLimit: 409600
}));
app.use(express.static('dist'));
// app.use(fileUpload());
app.disable('etag');
app.use(session({
  secret: '12sdfwerwersdfserwerwef', //keboard cat (랜덤한 값)
  resave: true,
  saveUninitialized: true
}));

app.use(transferLog);

// admin router를 설정한다.
require('./admin/router_factory')(app);
require('./user/router_factory')(app);


function clone(obj) {
  if (obj === null || typeof(obj) !== 'object')
    return obj;

  var copy = obj.constructor();

  for (var attr in obj) {
    if (obj.hasOwnProperty(attr)) {
      copy[attr] = obj[attr];
    }
  }
  return copy;
}


app.listen(port, () => console.log(`Listening on port ${port}`));

app.use('/', express.static(__dirname + '/views_main'));
// app.use('/upload',express.static('/uploads'));
