/**
 *  Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(typeId, title, content, postUser) {
  var obj = new Object();
  try {
    let result = await tableCall.board.create({
      typeId: typeId,
      title: title,
      content: content,
      postUser: postUser,
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Board Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Board Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.insertComment = async function(typeId, title, content, boardId, parentId, postUser) {
  var obj = new Object();
  try {
    let result = await tableCall.board.create({
      typeId: typeId,
      title: title,
      content: content,
      boardId: boardId,
      parentId: parentId,
      postUser: postUser,
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Board Comments Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Board Comments Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.update = async function(id, typeId, title, content, postUser) {
  var obj = new Object();
  try {
    let result = await tableCall.board.update({
      typeId: typeId,
      title: title,
      content: content,
    }, {
      where: {
        id: id,
        postUser: postUser,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Board Fail.';
    }

  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Board Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.board.update({
      deletionOrderer: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete Board Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete Board Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectByType = async function() {
  var obj = new Object();
  try {
    let result = await tableCall.board.findAll({
      where: {
        deletedAt: null,
        boardId: null,
        parentId: null,
      },
      group: 'typeId',
      attributes: ['typeId', 'title', [sequelize.fn('COUNT', 'typeId'), 'count']],
      raw: true
    })
    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Board by type List Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Board by type List Fail.';
    obj.error = err.message;
  }
  return obj
}


exports.selectList = async function(type, typeId, title, content, offset, limit) {
  console.log("@@@ selectList");
  console.log("type = " + type);
  console.log("typeId = " + typeId);
  console.log("title = " + title);
  console.log("content = " + content);
  console.log("offset = " + offset);
  console.log("limit = " + limit);
  
  
  var obj = new Object();
  try {
    let result = await tableCall.board.findAndCountAll({
      where: {
        deletedAt: null,
        boardId: null,
        parentId: null,
        // title: {
        //   [Op.like]: "%" + title + "%"
        // },
        // content: {
        //   [Op.like]: "%" + content + "%"
        // },
      },
      include: [{
          model: tableCall.type,
          as: 'type',
          required: true,
          where: {
            deletedAt: null,
            // id: {
            //   [Op.like]: "%" + typeId + "%"
            // },
            title: {
              [Op.like]: "%" + type + "%"
            }
          },
          attributes: ['id', 'title', 'description', 'created_at', 'updated_at'],
        },
        {
          model: tableCall.user,
          as: 'user',
          required: true,
          where: {
            deletedAt: null,
          },
          attributes: ['id', 'email', 'created_at', 'updated_at'],
        },
      ],
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'typeId', 'title', 'content', 'postUser', 'boardId', 'parentId', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
      distinct: true,
    })

    console.log("@@@ result = " + JSON.stringify(result));
    
    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Board List Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Board List Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.board.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      include: [{
          model: tableCall.user,
          required: false,
          as: 'user',
          where: {
            deletedAt: null,
          },
          attributes: ['id', 'email', 'created_at', 'updated_at'],
        },
        {
          model: tableCall.file,
          required: false,
          as: 'file',
          where: {
            deletedAt: null,
          },
          attributes: ['id', 'fieldname', 'originalname', 'encoding', 'mimetype', 'destination', 'filename', 'path', 'size', 'created_at', 'updated_at'],
        },
        {
          model: tableCall.type,
          required: false,
          as: 'type',
          where: {
            deletedAt: null,
          },
          attributes: ['id', 'title', 'description', 'created_at', 'updated_at'],
        },
        {
          model: tableCall.board,
          required: false,
          as: 'parentBoard',
          where: {
            deletedAt: null,
            parentId: id,
          },
          include: [{
              all: true
            },
            {
              model: tableCall.board,
              required: false,
              as: 'childBoard',
              where: {
                deletedAt: null,
              },
              include: [{
                all: true
              }, ]
            }
          ],
          attributes: ['id', 'typeId', 'title', 'content', 'postUser', 'boardId', 'parentId', 'created_at', 'updated_at'],
        },
      ],
      attributes: ['id', 'typeId', 'title', 'content', 'postUser', 'boardId', 'parentId', 'created_at', 'updated_at'],
      distinct: true
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Board by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Board by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
