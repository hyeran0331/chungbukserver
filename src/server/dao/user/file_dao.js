/**
 *  Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(
  fieldname,
  originalname,
  encoding,
  mimetype,
  destination,
  filename,
  path,
  size,
  postId
) {
  var obj = new Object();
  try {
    let result = await tableCall.file.create({
      fieldname: fieldname,
      originalname: originalname,
      encoding: encoding,
      mimetype: mimetype,
      destination: destination,
      filename: filename,
      path: path,
      size: size,
      postId: postId
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert file Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert file Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.file.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      include: [{
        model: tableCall.board,
        required: true,
        as: 'board',
        where: {
          deletedAt: null,
        },
        attributes: ['id', 'type', 'title', 'postUser', 'created_at', 'updated_at'],
      }],
      attributes: ['id', 'fieldname', 'originalname', 'encoding', 'mimetype', 'destination', 'filename', 'path', 'size', 'postId', 'created_at', 'updated_at'],
    })
    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search File by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search File by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
