/**
 * User Auth Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');

exports.findLoginUser = async function(email , password) {
  var obj = new Object();
  try {
    let loginUser = await tableCall.user.findOne({
      where: {
        email : email,
        password: password,
        deletedAt: null
      },
      attributes: [
        'id',
        'email',
        'backupEmail',
        'userName',
        'engFirstName',
        'engLastName',
        'gender',
        'job',
        'area',
        'phoneNumber',
        'generalNumber',
        'affiliation',
        'major',
        'position',
        'comTelephone',
        'comFax',
        'postalCode',
        'address',
        'detailAddress',
        'grade',
        'duty',
        'memo',
        'mailConfimedAt',
        'created_at',
        'updated_at'
      ], //object
    });

    if (loginUser) {
      obj.success = true;
      obj.data = loginUser;

    } else {
      obj.success = false;
      obj.reason = 'Not Find User.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Not Find User.';
    obj.error = err;
  }
  return obj
}

exports.updatePassword = async function(id, password, newPassword) {
  var obj = new Object();
  try {
    let result = await tableCall.user.update({
      password: newPassword
    }, {
      where: {
        id: id,
        password: password,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Password Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Password Error.';
    obj.error = err;
  }
  return obj
}

exports.updatePasswordByEmail = async function(email, password) {
  var obj = new Object();
  try {
    let result = await tableCall.user.update({
      password: password
    }, {
      where: {
        backupEmail: email,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Password Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Password Error.';
    obj.error = err;
  }
  return obj
}

exports.updateLoginAt = async function(id) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.update({
      loginAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Login Time Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Login Time Error.';
    obj.error = err;
  }
  return obj
}
