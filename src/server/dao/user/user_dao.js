/**
 * User Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insertUser = async function(
  email,
  backupEmail,
  password,
  userName,
  gender,
  phoneNumber,
  generalNumber,
  job,
  area,
  affiliation,
  major,
  position,
  comTelephone,
  comFax,
  postalCode,
  address,
  detailAddress,
  grade,
  duty
) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.create({
      email: email,
      backupEmail: backupEmail,
      password: password,
      userName: userName,
      gender: gender,
      phoneNumber: phoneNumber,
      generalNumber: generalNumber,
      job: job,
      area: area,
      affiliation: affiliation,
      major: major,
      position: position,
      comTelephone: comTelephone,
      comFax: comFax,
      postalCode: postalCode,
      address: address,
      detailAddress: detailAddress,
      grade: grade,
      duty: duty
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert User Fail.';
    obj.error = err;
  }
  return obj
}

exports.updateUser = async function(
  id,
  email,
  backupEmail,
  userName,
  engFirstName,
  engLastName,
  phoneNumber,
  generalNumber,
  job,
  area,
  affiliation,
  major,
  position,
  comTelephone,
  comFax,
  postalCode,
  address,
  detailAddress,
  memo,
  grade,
  duty
) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.update({
      backupEmail: backupEmail,
      userName: userName,
      engFirstName: engFirstName,
      engLastName: engLastName,
      phoneNumber: phoneNumber,
      generalNumber: generalNumber,
      job: job,
      area: area,
      affiliation: affiliation,
      major: major,
      position: position,
      comTelephone: comTelephone,
      comFax: comFax,
      postalCode: postalCode,
      address: address,
      detailAddress: detailAddress,
      memo: memo,
      grade: grade,
      duty: duty
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result) {
      if (result.length > 0 && result[0] != 0) {
        obj.success = true;
        obj.data = result;
      } else {
        obj.success = false;
        obj.reason = 'Update User Fail.';
      }
    } else {
      obj.success = false;
      obj.reason = 'Update User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update User Fail.';
    obj.error = err;
  }
  return obj
}

exports.updatePassword = async function(id, newPassword) {
  var obj = new Object();
  try {
    let result = await tableCall.user.update({
      password: newPassword
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Password Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Password Error.';
    obj.error = err;
  }
  return obj
}

exports.updateUserInfo = async function(id, grade, duty, memo) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.update({
      grade: grade,
      duty: duty,
      memo: memo
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update user info Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update user info Error.';
    obj.error = err;
  }
  return obj
}


exports.updateConfirm = async function(id, backupEmail) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.update({
      mailConfimedAt: utcDate
    }, {
      where: {
        id: id,
        backupEmail: backupEmail,
        mailConfimedAt: null,
        deletedAt: null
      }
    })

    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Confrim Information not matched.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Confrim Information Error.';
    obj.error = err;
  }
  return obj
}

exports.deleteUser = async function(id, password) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.update({
      deletionOrderer: id,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        password: password,
        deletedAt: null
      }
    })
    if (result) {
      if (result.length > 0 && result[0] != 0) {
        obj.success = true;
        obj.data = result;
      } else {
        obj.success = false;
        obj.reason = 'Delete User Fail.11';
      }
    } else {
      obj.success = false;
      obj.reason = 'Delete User Fail.22';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete User Fail.33';
    obj.error = err;
  }
  return obj
}

exports.selectUserById = async function(id) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.findOne({
      where: {
        deletedAt: null,
        id: id
      },
      attributes: [
        'id',
        'email',
        'backupEmail',
        'password',
        'userName',
        'engFirstName',
        'engLastName',
        'job',
        'area',
        'phoneNumber',
        'generalNumber',
        'affiliation',
        'major',
        'position',
        'comTelephone',
        'comFax',
        'postalCode',
        'address',
        'detailAddress',
        'grade',
        'duty',
        'memo',
        'created_at',
        'updated_at'
      ], //object
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User by Id Fail.';
    obj.error = err;
  }
  return obj
}

exports.selectUserByEmail = async function(email, backupEmail) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.user.findOne({
      where: {
        deletedAt: null,
        [Op.or]: [{email: email},
          {backupEmail: email},
          {email: backupEmail},
          {backupEmail: backupEmail}]
      },
      attributes: [
        'id',
        'email',
        'backupEmail',
        'userName',
        'phoneNumber',
        'affiliation',
        'grade',
        'created_at',
        'updated_at'
      ], //object
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User by Email Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User by Email Fail.';
    obj.error = err;
  }
  return obj
}

exports.selectUserList = async function(content, userName, phoneNumber, email, affiliation, grade, created_at, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.user.findAndCountAll({
      where: {
        deletedAt: null,
        [Op.or]: [{userName: {[Op.like]: "%" + content + "%"}},
          {phoneNumber: {[Op.like]: "%" + content + "%"}},
          {email: {[Op.like]: "%" + content + "%"}},
          {affiliation: {[Op.like]: "%" + content + "%"}},
          {grade: {[Op.like]: "%" + content + "%"}},
          {created_at: {[Op.like]: "%" + content + "%"}}
        ],
        userName: {[Op.like]: "%" + userName + "%"},
        phoneNumber: {[Op.like]: "%" + phoneNumber + "%"},
        email: {[Op.like]: "%" + email + "%"},
        affiliation: {[Op.like]: "%" + affiliation + "%"},
        grade: {[Op.like]: "%" + grade + "%"}
            },
      offset: Number(offset),
      limit: Number(limit),
      attributes: [
        'id',
        'email',
        'userName',
        'phoneNumber',
        'affiliation',
        'grade',
        'created_at',
        'updated_at'
      ],
      order: [
        ['created_at', 'DESC'],
      ],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User Fail.';
    obj.error = err;
  }
  return obj
}
