/**
 * TransferLog Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(
  position,
  title,
  bannerContent,
  target,
  link,
  bannerStartDate,
  bannerEndDate,
  exposure,
  turn,
  bannerFile
) {
  var obj = new Object();
  try {
    let result = await tableCall.banner.create({
      position: position,
      title: title,
      bannerContent: bannerContent,
      target: target,
      link: link,
      bannerStartDate: bannerStartDate,
      bannerEndDate: bannerEndDate,
      exposure: exposure,
      turn: turn,
      bannerFile: bannerFile
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert banner Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert banner Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.update = async function(
  id,
  position,
  title,
  bannerContent,
  target,
  link,
  bannerStartDate,
  bannerEndDate,
  exposure,
  turn,
  bannerFile
) {
  var obj = new Object();
  try {
    let result = await tableCall.banner.update({
      position: position,
      title: title,
      bannerContent: bannerContent,
      target: target,
      link: link,
      bannerStartDate: bannerStartDate,
      bannerEndDate: bannerEndDate,
      exposure: exposure,
      turn: turn,
      bannerFile: bannerFile
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update banner Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update banner Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.banner.update({
      deletionOrderer: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete banner Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete banner Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectList = async function(content, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.banner.findAndCountAll({
      where: {
        deletedAt: null,
        [Op.or]: [{
            position: {
              [Op.like]: "%" + content + "%"
            }
          },
          {
            title: {
              [Op.like]: "%" + content + "%"
            }
          },
          {
            link: {
              [Op.like]: "%" + content + "%"
            }
          },
          {
            exposure: {
              [Op.like]: "%" + content + "%"
            }
          },
        ],
      },
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'position', 'title', 'link', 'bannerStartDate', 'bannerEndDate', 'exposure', 'bannerFile', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search banner Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search banner Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.banner.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      attributes: ['id', 'position', 'title', 'bannerContent', 'target', 'link', 'bannerStartDate', 'bannerEndDate', 'exposure', 'turn', 'bannerFile', 'created_at', 'updated_at']
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search banner by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search banner by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
