/**
 * User Admin Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insertUser = async function(email, password, name, department, duty) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.admin.create({
      email: email,
      password: password,
      name: name,
      department: department,
      duty: duty
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert User Fail.';
    obj.error = err;
  }
  return obj
}

exports.updateUser = async function(id, name, department, duty) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.admin.update({
      name: name,
      department: department,
      duty: duty,
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result) {
      if (result.length > 0 && result[0] != 0) {
        obj.success = true;
        obj.data = result;
      } else {
        obj.success = false;
        obj.reason = 'Update User Fail.';
      }
    } else {
      obj.success = false;
      obj.reason = 'Update User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update User Fail.';
    obj.error = err;
  }
  return obj
}

exports.updatePassword = async function(id, newPassword) {
  var obj = new Object();
  try {
    let result = await tableCall.admin.update({
      password: newPassword
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })
    if (result.length > 0 && result[0] != 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Password Error.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Password Error.';
    obj.error = err;
  }
  return obj
}

exports.deleteUser = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.admin.update({
      deletedUser: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result) {
      if (result.length > 0 && result[0] != 0) {
        obj.success = true;
        obj.data = result;
      } else {
        obj.success = false;
        obj.reason = 'Delete User Fail.';
      }
    } else {
      obj.success = false;
      obj.reason = 'Delete User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete User Fail.';
    obj.error = err;
  }
  return obj
}

exports.selectUserByEmail = async function(email) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.admin.findOne({
      where: {
        deletedAt: null,
        email: email
      },
      attributes: [
        'id',
        'email',
        'name',
        'department',
        'loginAt',
        'duty'
      ], //object
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User by Email Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User by Email Fail.';
    obj.error = err;
  }
  return obj
}

exports.selectUserById = async function(id) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.admin.findOne({
      where: {
        deletedAt: null,
        id: id
      },
      attributes: [
        'id',
        'email',
        'name',
        'department',
        'loginAt',
        'duty'
      ], //object
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User by Id Fail.';
    obj.error = err;
  }
  return obj
}

exports.selectUserList = async function(email, name, department, duty, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.admin.findAndCountAll({
      where: {
        email: {
          [Op.like]: "%" + email + "%"
        },
        name: {
          [Op.like]: "%" + name + "%"
        },
        department: {
          [Op.like]: "%" + department + "%"
        },
        duty: {
          [Op.like]: "%" + duty + "%"
        },
        deletedAt: null
      },
      offset: Number(offset),
      limit: Number(limit),
      attributes: [
        'id',
        'email',
        'name',
        'department',
        'loginAt',
        'duty'
      ],
      order: [
        ['loginAt', 'DESC'],
      ],
      // plain: true,
      // raw: true,
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search User Fail.';
    obj.error = err;
  }
  return obj
}
