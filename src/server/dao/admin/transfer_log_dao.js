/**
 * TransferLog Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(type, loginType, user, path, header, param, body) {
  var obj = new Object();
  try {
    let result = await tableCall.transferLog.create({
      type: type,
      loginType: loginType,
      user: user,
      path: path,
      header: header,
      param: param,
      body: body
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Transfer Log Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Transfer Log Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.update = async function(id, type, loginType, user, path, header, param, body) {
  var obj = new Object();
  try {
    let result = await tableCall.transferLog.update({
      type: type,
      loginType: loginType,
      user: user,
      path: path,
      header: header,
      param: param,
      body: body
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Transfer Log Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Transfer Log Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.transferLog.update({
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete Transfer Log Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete Transfer Log Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectList = async function(content, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.transferLog.findAndCountAll({
      where: {
        deletedAt: null,
        [Op.or]: [{type: {[Op.like]: "%" + content + "%"}},
        {loginType: {[Op.like]: "%" + content + "%"}},
          {user: {[Op.like]: "%" + content + "%"}},
          {path: {[Op.like]: "%" + content + "%"}},
          {header: {[Op.like]: "%" + content + "%"}},
          {param: {[Op.like]: "%" + content + "%"}},
          {body: {[Op.like]: "%" + content + "%"}}
        ]
      },
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'type', 'loginType','user', 'path', 'header', 'param', 'body', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Transfer Log Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Transfer Log Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.transferLog.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      attributes: ['id', 'type', 'loginType','user', 'path', 'header', 'param', 'body', 'created_at', 'updated_at'],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Transfer Log by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Transfer Log by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
