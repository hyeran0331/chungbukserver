/**
 *  Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(title, description) {
  var obj = new Object();
  try {
    let result = await tableCall.type.create({
      title: title,
      description: description,
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Board Type Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Board Type Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.update = async function(id, title, description) {
  var obj = new Object();
  try {
    let result = await tableCall.type.update({
      title: title,
      description: description,
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Board Type Fail.';
    }

  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Board Type Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.type.update({
      deletionOrderer: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })
    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete Board Type Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete Board Type Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectList = async function(content, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.type.findAndCountAll({
      where: {
        deletedAt: null,
        [Op.or]: [
          {title: {[Op.like]: "%" + content + "%"}},
          {description: {[Op.like]: "%" + content + "%"}},
        ],
      },
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'title', 'description', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Board Type List Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Board Type List Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.type.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      attributes: ['id', 'title', 'description', 'created_at', 'updated_at'],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Board Type by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Board Type by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
