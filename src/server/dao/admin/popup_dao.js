/**
 * TransferLog Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insert = async function(
  type,
  title,
  target,
  link,
  popupStartDate,
  popupEndDate,
  displayLeft,
  displayTop,
  size,
  popupFile,
  popupContent,
  exposure
) {
  var obj = new Object();
  try {
    let result = await tableCall.popup.create({
      type: type,
      title: title,
      target: target,
      link: link,
      popupStartDate: popupStartDate,
      popupEndDate: popupEndDate,
      displayLeft: displayLeft,
      displayTop: displayTop,
      size: size,
      popupFile: popupFile,
      popupContent: popupContent,
      exposure: exposure
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Popup Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Popup Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.update = async function(
  id,
  type,
  title,
  target,
  link,
  popupStartDate,
  popupEndDate,
  displayLeft,
  displayTop,
  size,
  popupFile,
  popupContent,
  exposure
) {
  var obj = new Object();
  try {
    let result = await tableCall.popup.update({
      type: type,
      title: title,
      target: target,
      link: link,
      popupStartDate: popupStartDate,
      popupEndDate: popupEndDate,
      displayLeft: displayLeft,
      displayTop: displayTop,
      size: size,
      popupFile: popupFile,
      popupContent: popupContent,
      exposure: exposure
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Popup Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Popup Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.popup.update({
      deletionOrderer: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete Popup Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete Popup Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectList = async function(content, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.popup.findAndCountAll({
      where: {
        deletedAt: null,
        [Op.or]: [
          {title: {[Op.like]: "%" + content + "%"}},
          {exposure: {[Op.like]: "%" + content + "%"}},
        ],
      },
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'title','popupStartDate','popupEndDate','popupFile', 'exposure','created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Popup Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Popup Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.popup.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      attributes: ['id', 'type', 'title','target','link','popupStartDate','popupEndDate', 'displayLeft','displayTop','size','popupFile','popupContent','exposure','created_at', 'updated_at']
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search Popup by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search Popup by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
