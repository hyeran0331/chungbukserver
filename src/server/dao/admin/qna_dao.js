/**
 * Hotel Reservation Dao 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const tableCall = require('../../db/orm/table_call');
const sequelize = require('sequelize');
const Op = sequelize.Op;

exports.insertQuestion = async function(title, state, question, questionUserId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.qna.create({
      title: title,
      state: state,
      question: question,
      questionDate: utcDate,
      questionUserId: questionUserId,
    });

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Insert Question Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Insert Question Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.updateQuestion = async function(id, title, state, question, questionUserId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.qna.update({
      title: title,
      state: state,
      question: question,
      questionDate: utcDate,
    }, {
      where: {
        id: id,
        questionUserId: questionUserId,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Question Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Question Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.updateAnswer = async function(id, title, state, answer, answerUserId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.qna.update({
      title: title,
      state: state,
      answer: answer,
      answerDate: utcDate,
      answerUserId: answerUserId
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Update Answer Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Update Answer Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.deleteByUser = async function(id, state, questionUserId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.qna.update({
      state: state
    }, {
      where: {
        id: id,
        questionUserId: questionUserId,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete QnA By User Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete QnA By User Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.delete = async function(id, userId) {
  var obj = new Object();
  try {
    var utcDate = new Date(new Date().toUTCString());
    let result = await tableCall.qna.update({
      deletionOrderer: userId,
      deletedAt: utcDate
    }, {
      where: {
        id: id,
        deletedAt: null
      }
    })

    if (result > 0) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Delete QnA Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Delete QnA Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectList = async function(title, state, userName, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.qna.findAndCountAll({
      where: {
        deletedAt: null,
        title: {
          [Op.like]: "%" + title + "%"
        },
        state: {
          [Op.like]: "%" + state + "%"
        },
      },
      include: [{
        model: tableCall.user,
        as: 'user',
        required: true,
        where: {
          deletedAt: null,
          userName: {
            [Op.like]: "%" + userName + "%"
          },
        },
        attributes: ['id', 'email', 'backupEmail', 'userName', 'phoneNumber', 'created_at', 'updated_at'],
      }, ],
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'title', 'state', 'questionDate', 'answerDate', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
      distinct: true,
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search QnA List Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search QnA List Fail.';
    obj.error = err.message;
  }
  return obj
}
exports.selectQuestionUserList = async function(questionUserId, title, state, offset, limit) {
  var obj = new Object();
  try {
    let result = await tableCall.qna.findAndCountAll({
      where: {
        deletedAt: null,
        questionUserId: questionUserId,
        title: {[Op.like]: "%" + title + "%"},
        state: {[Op.like]: "%" + state + "%"},
      },
      include: [{
        model: tableCall.user,
        as: 'user',
        required: true,
        where: {
          deletedAt: null,
        },
        attributes: ['id', 'email', 'backupEmail', 'userName', 'phoneNumber', 'created_at', 'updated_at'],
      }, ],
      offset: Number(offset),
      limit: Number(limit),
      attributes: ['id', 'title', 'state', 'questionDate', 'answerDate', 'created_at', 'updated_at'],
      order: [
        ['id', 'DESC']
      ],
      distinct: true,
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search QnA List by QuestionUserId Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search QnA List by QuestionUserId Fail.';
    obj.error = err.message;
  }
  return obj
}

exports.selectById = async function(id) {
  var obj = new Object();
  try {
    let result = await tableCall.qna.findOne({
      where: {
        id: id,
        deletedAt: null
      },
      include: [{
        model: tableCall.user,
        required: false,
        as: 'user',
        where: {
          deletedAt: null,
        },
        attributes: ['id', 'email', 'backupEmail', 'userName', 'phoneNumber', 'created_at', 'updated_at'],
      }, ],
      attributes: ['id', 'title', 'state', 'question', 'questionDate', 'questionUserId', 'answer', 'answerDate', 'answerUserId', 'created_at', 'updated_at']
    })

    if (result) {
      obj.success = true;
      obj.data = result;
    } else {
      obj.success = false;
      obj.reason = 'Search QnA by Id Fail.';
    }
  } catch (err) {
    console.log(err);
    obj.success = false;
    obj.reason = 'Search QnA by Id Fail.';
    obj.error = err.message;
  }
  return obj
}
