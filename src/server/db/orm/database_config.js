var serverConfig  = require('../../config.json');
var Sequelize = require('sequelize');
var sequelize = new Sequelize(serverConfig.db_name, serverConfig.db_user, serverConfig.db_password, {
	  host: serverConfig.db_host,
	  dialect: 'mysql',
		logging: serverConfig.db_logging,
	  pool: {
	    max: 5,
	    min: 0,
	    acquire: 30000,
	    idle: 10000
	  },
	   // SQLite only
	  storage: 'path/to/database.sqlite',
	   // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
	  operatorsAliases: false
	});

	// var sequelize = new Sequelize('connect', 'connect', 'hiconnecttech!!', {
	// 	  host: 'holichsoft.com',
	// 	  dialect: 'mysql',
	// 		logging: false,
	// 	  pool: {
	// 	    max: 5,
	// 	    min: 0,
	// 	    acquire: 30000,
	// 	    idle: 10000
	// 	  },
	// 	   // SQLite only
	// 	  storage: 'path/to/database.sqlite',
	// 	   // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
	// 	  operatorsAliases: false
	// 	});

	// var sequelize = new Sequelize('myconect', 'root', 'racos5117', {
	// 	  host: 'localhost',
	// 	  dialect: 'mysql',
	// 		logging: false,
	// 	  pool: {
	// 	    max: 5,
	// 	    min: 0,
	// 	    acquire: 30000,
	// 	    idle: 10000
	// 	  },
	// 	   // SQLite only
	// 	  storage: 'path/to/database.sqlite',
	// 	   // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
	// 	  operatorsAliases: false
	// 	});


module.exports = sequelize;
