
var sequelize = require('./database_config');


const type = require('./tables/admin/type')(sequelize);

// User Table 생성.
const user = require('./tables/user/user')(sequelize);
const board = require('./tables/user/board')(sequelize, user, type);
const file = require('./tables/user/file')(sequelize, board);


// Admin Table 생성.
const admin = require('./tables/admin/admin')(sequelize);
const transfer_log = require('./tables/admin/transfer_log')(sequelize);
const popup = require('./tables/admin/popup')(sequelize);
const banner = require('./tables/admin/banner')(sequelize);
const qna = require('./tables/admin/qna')(sequelize, user);


// User Table
exports.user = user;
exports.board = board;
exports.file = file;

// Admin Table
exports.admin = admin;
exports.transferLog = transfer_log;
exports.popup = popup;
exports.banner = banner;
exports.qna = qna;
exports.type = type;



function getUser(result){
	result(user);
}
exports.getUser = getUser;

async function getDateLog(day){
	var dateLog = require('./tables/admin/transfer_log')(sequelize, day);
	await dateLog.sync({force: false}).then(() => {});
	return dateLog
}
exports.dateLog = getDateLog;
