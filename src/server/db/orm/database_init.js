/**
 * http://usejsdoc.org/
 */
async function initDatabase(){
	var sequelize = require('./database_config');

	// User Table 생성.
	const user = require('./tables/user/user')(sequelize);
	await user.sync({force: false}).then(() => {});

	const type = require('./tables/admin/type')(sequelize);
	await type.sync({force: false}).then(() => {});

	const board = require('./tables/user/board')(sequelize, user, type);
	await board.sync({force: false}).then(() => {});

	const file = require('./tables/user/file')(sequelize, board);
	await file.sync({force: false}).then(() => {});

	// Admin Table 생성.
	const admin = require('./tables/admin/admin')(sequelize);
	await admin.sync({force: false}).then(() => {});

	const transfer_log = require('./tables/admin/transfer_log')(sequelize);
	await transfer_log.sync({force: false}).then(() => {});

	const popup = require('./tables/admin/popup')(sequelize);
	await popup.sync({force: false}).then(() => {});

	const banner = require('./tables/admin/banner')(sequelize);
	await banner.sync({force: false}).then(() => {});

	const qna = require('./tables/admin/qna')(sequelize, user);
	await qna.sync({force: false}).then(() => {});



	admin.findAll({
		where: {
			deletedAt: null
		}
	}).then(function(results) {
		console.log('results = ' + results.length)
		if(results.length <= 0){
			const crypto = require('crypto')
			let hashPassword = crypto.createHash("sha512").update("holichannel1@").digest("base64");
			admin.create({
				email: "bhpark@holich.net",
	      password: hashPassword,
	      name: "박병호",
	      department: "회사",
	      duty: "ADMIN"
			}).then(function(results) {
				console.log(results);
			}).catch(function(err) {
				console.log(err);
			});
		}
	}).catch(function(err) {
		console.log(err);
	});


}

exports.initDatabase = initDatabase;
