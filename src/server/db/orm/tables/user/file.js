const Sequelize = require('sequelize');

module.exports = function(sequelize, Board) {
  const file = sequelize.define('File', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    fieldname: {
      field: 'fieldname',
      type: Sequelize.STRING,
      allowNull: true
    },
    originalname: {
      field: 'originalname',
      type: Sequelize.STRING,
      allowNull: true
    },
    encoding: {
      field: 'encoding',
      type: Sequelize.STRING,
      allowNull: true
    },
    mimetype: {
      field: 'mimetype',
      type: Sequelize.STRING,
      allowNull: true
    },
    destination: {
      field: 'destination',
      type: Sequelize.STRING,
      allowNull: true
    },
    filename: {
      field: 'filename',
      type: Sequelize.STRING,
      allowNull: true
    },
    path: {
      field: 'path',
      type: Sequelize.STRING,
      allowNull: true
    },
    size: {
      field: 'size',
      type: Sequelize.STRING,
      allowNull: true
    },
    postId: {
      field: 'post_id',
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: Board,
        key: 'id',
      }
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    underscored: true,
    freezeTableName: true,
    tableName: 'ct_file'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  file.belongsTo(Board, {
    as: 'board',
    foreignKey: 'postId'
  });
  Board.hasMany(file, {
    as: 'file',
    foreignKey: 'postId'
  });

  return file;
};
