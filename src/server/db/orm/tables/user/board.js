const Sequelize = require('sequelize');

module.exports = function(sequelize, User, Type) {
  const board = sequelize.define('Board', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    typeId: {
      field: 'type_id',
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: Type,
        key: 'id',
      }
    },
    title: {
      field: 'title',
      type: Sequelize.STRING,
      allowNull: true
    },
    content: {
      field: 'content',
      type: Sequelize.TEXT('long'),
      allowNull: true
    },
    postUser: {
      field: 'post_user',
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: User,
        key: 'id',
      }
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    underscored: true,
    freezeTableName: true,
    tableName: 'ct_board'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  board.belongsTo(User, {
    as: 'user',
    foreignKey: 'postUser'
  });
  User.hasMany(board, {
    as: 'board',
    foreignKey: 'postUser'
  });
  board.hasMany(board, {
    as: 'parentBoard',
    foreignKey: 'boardId'
  })
  board.hasMany(board, {
    as: 'childBoard',
    foreignKey: 'parentId'
  })
  board.belongsTo(Type, {
    as: 'type',
    foreignKey: 'typeId'
  });
  Type.hasMany(board, {
    as: 'board',
    foreignKey: 'typeId'
  });

  return board;
};
