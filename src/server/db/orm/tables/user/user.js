const Sequelize = require('sequelize');

module.exports = function(sequelize) {
  const user = sequelize.define('User', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    email: {
      field: 'email',
      type: Sequelize.STRING,
      allowNull: false
    },
    backupEmail: {
      field: 'backup_mail',
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      field: 'password',
      type: Sequelize.STRING,
      allowNull: false
    },
    userName: {
      field: 'user_name',
      type: Sequelize.STRING,
      allowNull: false
    },
    engFirstName: {
      field: 'eng_first_name',
      type: Sequelize.STRING,
      allowNull: true
    },
    engLastName: {
      field: 'eng_last_name',
      type: Sequelize.STRING,
      allowNull: true
    },
    gender: {
      field: 'gender',
      type: Sequelize.STRING,
      allowNull: true
    },
    job: {
      field: 'job',
      type: Sequelize.STRING,
      allowNull: true
    },
    area: {
      field: 'area',
      type: Sequelize.STRING,
      allowNull: true
    },
    phoneNumber: {
      field: 'phone_number',
      type: Sequelize.STRING,
      allowNull: false
    },
    generalNumber: {
      field: 'general_number',
      type: Sequelize.STRING,
      allowNull: true
    },
    affiliation: {
      field: 'affiliation',
      type: Sequelize.STRING,
      allowNull: true
    },
    major: {
      field: 'major',
      type: Sequelize.STRING,
      allowNull: true
    },
    position: {
      field: 'position',
      type: Sequelize.STRING,
      allowNull: true
    },
    comTelephone: {
      field: 'com_telephone',
      type: Sequelize.STRING,
      allowNull: true
    },
    comFax: {
      field: 'com_fax',
      type: Sequelize.STRING,
      allowNull: true
    },
    postalCode: {
      field: 'postal_code',
      type: Sequelize.STRING,
      allowNull: true
    },
    address: {
      field: 'address',
      type: Sequelize.STRING,
      allowNull: true
    },
    detailAddress: {
      field: 'detail_address',
      type: Sequelize.STRING,
      allowNull: true
    },
    grade: {
      field: 'grade',
      type: Sequelize.STRING,
      allowNull: true
    },
    duty: {
      field: 'duty',
      type: Sequelize.STRING,
      allowNull: true
    },
    memo: {
      field: 'memo',
      type: Sequelize.STRING,
      allowNull: true
    },
    lockedAt: {
      field: 'locked_at',
      type: Sequelize.DATE,
      allowNull: true
    },
    mailConfimedAt: {
      field: 'mail_confirmed_at',
      type: Sequelize.DATE,
      allowNull: true
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  },{
    // don't use camelcase for automatically added attributes but underscore
    // style
    // so updatedAt will be updated_at
    underscored: true,

    // disable the modification of tablenames; By default, sequelize will
    // automatically
    // transform all passed model names (first parameter of define) into
    // plural.
    // if you don't want that, set the following
    freezeTableName: true,

    // define the table's name
    tableName: 'ct_user'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  return user;
};
