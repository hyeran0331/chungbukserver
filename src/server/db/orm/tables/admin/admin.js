const Sequelize = require('sequelize');

module.exports = function(sequelize) {
	const admin = sequelize.define('Admin', {
		id : {
			field : 'id',
			type : Sequelize.INTEGER,
			autoIncrement: true,
			allowNull : false,
			primaryKey: true
		},
		email : {
			field : 'email',
			type : Sequelize.STRING,
			allowNull : false
		},
		password : {
			field : 'password',
			type : Sequelize.STRING,
			allowNull : false
		},
		name : {
			field : 'name',
			type : Sequelize.STRING,
			allowNull : true
		},
		department : {
			field : 'department',
			type : Sequelize.STRING,
			allowNull : true
		},
		accessToken : {
			field : 'access_token',
			type : Sequelize.STRING,
			allowNull : true
		},
		loginAt : {
			field : 'login_at',
			type : Sequelize.DATE,
			allowNull : true
		},
		duty : {
			field : 'duty',
			type : Sequelize.STRING,
			allowNull : true
		},
		deletedUser : {
			field : 'deleted_user',
			type : Sequelize.INTEGER,
			allowNull : true
		},
		deletedAt : {
			field : 'deleted_at',
			type : Sequelize.DATE,
			allowNull : true
		},
	},
	{
		underscored : true,
		freezeTableName : true,
		tableName : 'ct_admin'
	},{
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

	return admin;
};
