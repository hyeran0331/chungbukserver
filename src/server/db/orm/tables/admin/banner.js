const Sequelize = require('sequelize');

module.exports = function(sequelize) {
  const banner = sequelize.define('Banner', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    position: {
      field: 'position',
      type: Sequelize.STRING,
      allowNull: true
    },
    title: {
      field: 'title',
      type: Sequelize.STRING,
      allowNull: true
    },
    bannerContent: {
      field: 'banner_content',
      type: Sequelize.STRING,
      allowNull: true
    },
    target: {
      field: 'target',
      type: Sequelize.STRING,
      allowNull: true
    },
    link: {
      field: 'link',
      type: Sequelize.STRING,
      allowNull: true
    },
    bannerStartDate: {
      field: 'banner_start_date',
      type: Sequelize.DATE,
      allowNull: true
    },
    bannerEndDate: {
      field: 'banner_end_date',
      type: Sequelize.DATE,
      allowNull: true
    },
    exposure: {
      field: 'exposure',
      type: Sequelize.STRING,
      allowNull: true
    },
    turn: {
      field: 'turn',
      type: Sequelize.STRING,
      allowNull: true
    },
    bannerFile: {
      field: 'banner_file',
      type: Sequelize.TEXT('long'),
      allowNull: true
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    underscored: true,
    freezeTableName: true,
    tableName: 'ct_banner'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
  return banner;
};
