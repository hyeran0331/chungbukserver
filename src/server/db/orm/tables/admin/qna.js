const Sequelize = require('sequelize');

module.exports = function(sequelize, User) {
  const qna = sequelize.define('QnA', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    title: {
      field: 'title',
      type: Sequelize.STRING,
      allowNull: true
    },
    state: {
      field: 'state',
      type: Sequelize.STRING,
      allowNull: true
    },
    question: {
      field: 'question',
      type: Sequelize.STRING(1234),
      allowNull: true
    },
    questionDate: {
      field: 'question_date',
      type: Sequelize.DATE,
      allowNull: true
    },
    questionUserId: {
      field: 'question_user_id',
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: User,
        key: 'id',
      }
    },
    answer: {
      field: 'answer',
      type: Sequelize.STRING(1234),
      allowNull: true
    },
    answerDate: {
      field: 'answer_date',
      type: Sequelize.DATE,
      allowNull: true
    },
    answerUserId: {
      field: 'answer_user_id',
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: User,
        key: 'id',
      }
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    underscored: true,
    freezeTableName: true,
    tableName: 'ct_qna'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });

  qna.belongsTo(User, {
    as: 'user',
    foreignKey: 'questionUserId'
  });
  User.hasMany(qna, {
    as: 'qna',
    foreignKey: 'questionUserId'
  });

  return qna;
};
