const Sequelize = require('sequelize');

module.exports = function(sequelize) {
  const type = sequelize.define('Type', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    title: {
      field: 'title',
      type: Sequelize.STRING,
      allowNull: true
    },
    description: {
      field: 'description',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletionOrderer: {
      field: 'delete_orderer',
      type: Sequelize.STRING,
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    underscored: true,
    freezeTableName: true,
    tableName: 'ct_type'
  }, {
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
  return type;
};
