const Sequelize = require('sequelize');

module.exports = function(sequelize) {
  const transferLog = sequelize.define('TransferLog', {
    id: {
      field: 'id',
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true
    },
    type: {
      field: 'type',
      type: Sequelize.STRING,
      allowNull: true
    },
    loginType: {
      field: 'login_type',
      type: Sequelize.STRING,
      allowNull: true
    },
    user: {
      field: 'user',
      type: Sequelize.STRING,
      allowNull: true
    },
    path: {
      field: 'path',
      type: Sequelize.STRING,
      allowNull: true
    },
    header: {
      field: 'header',
      type: Sequelize.STRING(3000),
      allowNull: true
    },
    param: {
      field: 'param',
      type: Sequelize.STRING,
      allowNull: true
    },
    body: {
      field: 'body',
      type: Sequelize.STRING(3000),
      allowNull: true
    },
    deletedAt: {
      field: 'deleted_at',
      type: Sequelize.DATE,
      allowNull: true
    },
  }, {
    // don't use camelcase for automatically added attributes but underscore
    // style
    // so updatedAt will be updated_at
    underscored: true,

    // disable the modification of tablenames; By default, sequelize will
    // automatically
    // transform all passed model names (first parameter of define) into
    // plural.
    // if you don't want that, set the following
    freezeTableName: true,

    // define the table's name
    tableName: 'ct_transfer_log'
  });

  return transferLog;
};
