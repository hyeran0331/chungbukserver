const Sequelize = require('sequelize');

module.exports = function(sequelize) {
	const popup = sequelize.define('Popup', {
		id : {
			field : 'id',
			type : Sequelize.INTEGER,
			autoIncrement: true,
			allowNull : false,
			primaryKey: true
		},
		type : {
			field : 'type',
			type : Sequelize.STRING,
			allowNull : true
		},
		title : {
			field : 'title',
			type : Sequelize.STRING,
			allowNull : true
		},
    target : {
      field : 'target',
      type : Sequelize.STRING,
      allowNull : true
    },
    link : {
      field : 'link',
      type : Sequelize.STRING,
      allowNull : true
    },
    popupStartDate : {
      field : 'popup_start_date',
      type : Sequelize.DATE,
      allowNull : true
    },
    popupEndDate : {
      field : 'popup_end_date',
      type : Sequelize.DATE,
      allowNull : true
    },
    displayLeft : {
      field : 'display_left',
      type : Sequelize.STRING,
      allowNull : true
    },
		displayTop : {
			field : 'display_top',
			type : Sequelize.STRING,
			allowNull : true
		},
    size : {
      field : 'size',
      type : Sequelize.STRING,
      allowNull : true
    },
    popupFile : {
      field : 'popup_file',
      type : Sequelize.TEXT('long'),
      allowNull : true
    },
    popupContent : {
      field : 'popup_content',
      type : Sequelize.STRING,
      allowNull : true
    },
    exposure : {
      field : 'exposure',
      type : Sequelize.STRING,
      allowNull : true
    },
		deletionOrderer : {
			field : 'delete_orderer',
			type : Sequelize.STRING,
			allowNull : true
		},
		deletedAt : {
			field : 'deleted_at',
			type : Sequelize.DATE,
			allowNull : true
		},
	},
	{
		underscored : true,
		freezeTableName : true,
		tableName : 'ct_popup'
	},
	{
    charset: 'utf8',
    collate: 'utf8_unicode_ci'
  });
	return popup;
};
