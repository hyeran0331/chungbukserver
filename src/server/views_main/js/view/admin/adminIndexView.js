//AdminUser menu
var adminUserLi = '<a href="adminUserList.html" class="pageLink admin_user" onClick="menuLink($(this))">Admin User</a>'

//menu에 userinfo 보여주기
function addMenuInfo(name, department, email) {
  var menuInfo = '<span class="username" id="userName">' + name + '</span>';
  menuInfo += '<span class="usercompany" id="userCompany">' + department + '</span>';
  menuInfo += '<span class="useremail blind" id="userEmail">' + email + '</span>';
  $('.info_text').append(menuInfo);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    document.iframeContents.location.reload();
  }
}
