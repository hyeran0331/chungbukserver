// 사용자 리스트 추가하기
function addTypeList(id, title, description, date, index) {
  // var typeRow = '<div class="row tbodyRow" onClick="linkTypeDetail('+id+')">';
  var typeRow = '<div class="row tbodyRow" onClick="linkTypeDetail('+id+',\''+title+'\')">';
  typeRow += '<div class="cell">',
    typeRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    typeRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    typeRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    typeRow += '</label>',
    typeRow += '</div>',
    typeRow += '<div class="cell">' + index + '</div>',
    typeRow += '<div class="cell">' + title + '</div>',
    typeRow += '<div class="cell">' + description + '</div>',
    typeRow += '</div>'

  $('.tbody').append(typeRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#board_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var content = "";

  if (selectVal == 'content') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'typeList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
