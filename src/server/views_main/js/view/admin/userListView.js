// 사용자 리스트 추가하기
function addUserList(id, userName, email, affiliation, grade, phoneNumber, date, index) {
  var userRow = '<div class="row tbodyRow" onClick="linkUserDetail(' + id + ')">';
  userRow += '<div class="cell">',
    userRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    userRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    userRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    userRow += '</label>',
    userRow += '</div>',
    userRow += '<div class="cell">' + index + '</div>',
    userRow += '<div class="cell">' + userName + '</div>',
    userRow += '<div class="cell">' + email + '</div>',
    userRow += '<div class="cell">' + affiliation + '</div>',
    userRow += '<div class="cell">' + grade + '</div>',
    userRow += '<div class="cell">' + phoneNumber + '</div>',
    userRow += '<div class="cell">' + date + '</div>',
    userRow += '</div>'

  $('.tbody').append(userRow);
}


//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#user_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var content = ""
  var userName = ""
  var phoneNumber = ""
  var email = ""
  var affiliation = ""
  var grade = ""


  if (selectVal == 'content') {
    content = inputVal
  } else if (selectVal == 'userName') {
    userName = inputVal
  } else if (selectVal == 'phoneNumber') {
    phoneNumber = inputVal
  } else if (selectVal == 'email') {
    email = inputVal
  } else if (selectVal == 'affiliation') {
    affiliation = inputVal
  } else if (selectVal == 'grade') {
    grade = inputVal
  }
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'userList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
