// 사용자 리스트 추가하기
function addbannerList(id, position, title, link, bannerStartDate, bannerEndDate, exposure, bannerFileUrl, index){
  var bannerRow = '<div class="row tbodyRow" onClick="linkBannerDetail(' + id + ')">';
  bannerRow += '<div class="cell">',
    bannerRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    bannerRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    bannerRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    bannerRow += '</label>',
    bannerRow += '</div>',
    bannerRow += '<div class="cell">' + position + '</div>',
    bannerRow += '<div class="cell image_cell"><img src="'+bannerFileUrl+'" alt="banner file"></div>',
    bannerRow += '<div class="cell">' + title + '</div>',
    bannerRow += '<div class="cell">' + link + '</div>',
    bannerRow += '<div class="cell">' + bannerStartDate + '</div>',
    bannerRow += '<div class="cell">' + bannerEndDate + '</div>',
    bannerRow += '<div class="cell">' + exposure + '</div>',
    bannerRow += '</div>'

  $('.tbody').append(bannerRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#banner_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var content = "";


  if (selectVal == 'content') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'bannerList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
