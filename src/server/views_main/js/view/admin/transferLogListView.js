// 사용자 리스트 추가하기
function addTransferList(id, type, loginType,user, path, status, createdAt, index) {
  var transferRow = '<div class="row" onClick="linkTransferDetail(' + id + ')">';
  transferRow += '<div class="cell">',
    transferRow += '<div class="eachIndexNo"><span>' + index + '</span></div>',
    transferRow += '</div>',
    transferRow += '<div class="cell">' + type + '</div>',
    transferRow += '<div class="cell">' + loginType + '</div>',
    transferRow += '<div class="cell"> ' + user + '</div>',
    transferRow += '<div class="cell">' + path + '</div>',
    transferRow += '<div class="cell">' + status + '</div>',
    transferRow += '<div class="cell">' + createdAt + '</div>',
    transferRow += '</div>'

  $('.tbody').append(transferRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var fromDate = $('#termStartPicker').val();
  var endDate = $('#termEndPicker').val();
  var selectVal = $('#transferlist_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();

  var content = ""

  if (selectVal == 'content') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    // document.location.href = 'transferLogList.html?fromDate=' + fromDate + '&endDate=' + endDate + '&inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
    document.location.href = 'transferLogList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;

  }
}
