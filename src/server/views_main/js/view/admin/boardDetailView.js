var imageCon = '<img src="" alt="">'
var videoCon = '<video src="" autoplay></video>'

// fileInfo 보여주기
function addFileInfo(filename, filesize, mimetype, downloadPath) {
  var fileInfo = '<div class="file_info">';
  fileInfo += '<div class="row"><span>filename</span><p>' + filename + '</p></div>';
  fileInfo += '<div class="row"><span>filesize</span><p>' + filesize + '</p></div>';
  fileInfo += '<div class="row"><span>mimetype</span><p>' + mimetype + '</p></div>';
  fileInfo += '<div class="row"><a target="_self" href="' + downloadPath + '" download="' + filename + '">Download</a></div>';
  fileInfo += '</div>';
  $('.file_data').append(fileInfo);
}

// commentInfo 보여주기
function addCommentInfo(id, type, title, content, postUser, boardId, parentId, date, userName, userEmail) {
  var commentInfo = '<li class="user_cbox_comment">';
  commentInfo += '<div class="user_cbox_comment_box">';
  commentInfo += '<div class="user_cbox_area comment_id_'+id+'">';
  commentInfo += '<span class="user_cbox_comment_name">'+userName+'</span>';
  commentInfo += '<span class="user_cbox_comment_date">'+date+'</span>';
  commentInfo += '<span class="user_cbox_comment_content">'+content+'</span>';
  commentInfo += '<div class="user_cbox_reply_add">';
  commentInfo += '<button class="user_cbox_reply_add_btn btn_'+id+'" type="button" name="button" onclick="showAddReply('+id+')">AddReply</button>';
  commentInfo += '</div>';
  commentInfo += '</div>';
  commentInfo += '</div>';
  commentInfo += '<div class="user_cbox_reply_area">';
  commentInfo += '<ul class="reply_list_'+id+'">';
  commentInfo += '</ul>';
  commentInfo += '</div>';
  commentInfo += '</li>';
  $('.user_cbox_list').append(commentInfo);
}

// replyInfo 보여주기
function addReplyInfo(id, type, title, content, postUser, boardId, parentId, date, userName, userEmail) {
  var replyInfo = '<li class="user_cbox_comment reply_comment">';
  replyInfo += '<div class="user_cbox_comment_box reply_comment_box">';
  replyInfo += '<div class="user_cbox_area reply_area comment_id_'+id+'">';
  replyInfo += '<span class="user_cbox_comment_name reply_comment_name">'+userName+'</span>';
  replyInfo += '<span class="user_cbox_comment_date reply_comment_date">'+date+'</span>';
  replyInfo += '<span class="user_cbox_comment_content reply_comment_content">'+content+'</span>';
  replyInfo += '</div>';
  replyInfo += '</div>';
  replyInfo += '</li>';
  $('.reply_list_'+parentId+'').append(replyInfo);
}

//addReply box 보여주기
function addReplyBox() {
  var replyBox = '<li class="user_cbox_comment reply_comment">';
  replyBox += '<div class="user_cbox_comment_box reply_comment_box">';
  replyBox += '<div class="user_cbox_area reply_area comment_id_'+id+'">';
  replyBox += '<span class="user_cbox_comment_name reply_comment_name">'+userName+'</span>';
  replyBox += '<span class="user_cbox_comment_date reply_comment_date">'+date+'</span>';
  replyBox += '<span class="user_cbox_comment_content reply_comment_content">'+content+'</span>';
  replyBox += '</div>';
  replyBox += '</div>';
  replyBox += '</li>';
  $('.reply_list_'+parentId+'').append(replyBox);
}

//addReply box 보여주기
function showAddReply(id) {
  console.log("id = " + id);
  $('.btn_'+id+'').addClass('hidden');

  var showAdd = '<div class="user_cbox_reply_write_wrap">';
  showAdd += '<form>';
  showAdd += '<div class="user_cbox_reply_write">';
  showAdd += '<div class="summernote_reply"></div>';
  showAdd += '</div>';
  showAdd += '<div class="user_cbox_reply_upload">';
  showAdd += '<button class="user_cbox_reply_upload_btn" type="button" name="button">AddReply</button>';
  showAdd += '</div>';
  showAdd += '</form>';
  showAdd += '</div>';
  $('.comment_id_'+id+'').append(showAdd);

  //summernote 부분
  $('.summernote_reply').summernote({
    height: 30,
    minHeight: null,
    maxHeight: 100,
    focus: true,
    lang: 'ko-KR',
    placeholder: 'Please enter the reply.',
    toolbar: null,
  });

  $('.user_cbox_reply_upload_btn').click(function(){
    addReply(id, boardType)
  })

}


//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    document.location.reload();
  }
}
