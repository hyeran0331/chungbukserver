// 사용자 리스트 추가하기
function addPopupList(id, title, popupStartDate, popupEndDate, imageURL, exposure, index) {
  var popupRow = '<div class="row tbodyRow" onClick="linkPopupDetail(' + id + ')">';
  popupRow += '<div class="cell">',
    popupRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    popupRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    popupRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    popupRow += '</label>',
    popupRow += '</div>',
    popupRow += '<div class="cell">' + index + '</div>',
    popupRow += '<div class="cell image_cell"><img src="'+imageURL+'" alt="popup file"></div>',
    popupRow += '<div class="cell">' + title + '</div>',
    popupRow += '<div class="cell">' + popupStartDate + '</div>',
    popupRow += '<div class="cell">' + popupEndDate + '</div>',
    popupRow += '<div class="cell">' + exposure + '</div>',
    popupRow += '</div>'

  $('.tbody').append(popupRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#popup_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var content = "";


  if (selectVal == 'content') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'popupList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
