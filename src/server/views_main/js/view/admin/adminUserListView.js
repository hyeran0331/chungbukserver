// 사용자 리스트 추가하기
function addUserList(id, name, email, department, duty, date, index) {
  var userRow = '<div class="row tbodyRow" onClick="linkUserDetail(' + id + ')">';
  userRow += '<div class="cell">',
    userRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    userRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    userRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    userRow += '</label>',
    userRow += '</div>',
    userRow += '<div class="cell">' + name + '</div>',
    userRow += '<div class="cell">' + email + '</div>',
    userRow += '<div class="cell">' + department + '</div>',
    userRow += '<div class="cell">' + duty + '</div>',
    userRow += '<div class="cell">' + date + '</div>',
    userRow += '</div>'

  $('.tbody').append(userRow);
}


//iframe reload
document.onkeydown = trapRefresh;
function trapRefresh() {
  var selectVal = $('#userlist_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var name = ""
  var email = ""
  var department = ""
  var duty = ""

  if (selectVal == 'name') {
    name = inputVal
  } else if (selectVal == 'email') {
    email = inputVal
  } else if (selectVal == 'department') {
    department = inputVal
  } else if (selectVal == 'duty') {
    duty = inputVal
  }
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'adminUserList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
