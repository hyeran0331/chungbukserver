// 사용자 리스트 추가하기
function addBoardList(id, type, title, postUser, date, index) {
  var boardRow = '<div class="row tbodyRow" onClick="linkBoardDetail(' + id + ')">';
  boardRow += '<div class="cell">',
    boardRow += '<div class="eachIndexNo blind"><span>' + index + '</span></div>',
    boardRow += '<input type="checkbox" name="eachCheck" id="' + id + '"  onClick="inputClick(event, $(this))">',
    boardRow += '<label class="tbodyLabel" for="' + id + '" onClick="labelClick(event, $(this))">',
    boardRow += '</label>',
    boardRow += '</div>',
    boardRow += '<div class="cell">' + index + '</div>',
    boardRow += '<div class="cell">' + type + '</div>',
    boardRow += '<div class="cell">' + title + '</div>',
    boardRow += '<div class="cell">' + postUser + '</div>',
    boardRow += '<div class="cell">' + date + '</div>',
    boardRow += '</div>'

  $('.tbody').append(boardRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#freeboard_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var title = "";
  var content = "";

  if (selectVal == 'title') {
    title = inputVal
  }
  if (selectVal == 'content') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'typeList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
