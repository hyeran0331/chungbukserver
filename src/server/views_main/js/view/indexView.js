var signLink = '<div class="sign_link_form"><a href="signIn.html" class="sign_link"><div class="index_box"><p>Sign In</p><p>Click</p></div></a><a href="signUp.html" class="sign_link"><div class="index_box"><p>Sign Up</p><p>Click</p></div></a></div>'
var mainLink = '<div class="sign_link_form"><a href="main.html" class="main_link"><div class="index_box"><p>MAIN PAGE</p><p>Go to main page</p></div></a><div class="index_box" onClick="userLogout()"><p>LOGOUT</p><p>Click</p></div></div>'
var adminLink = '<a href="admin/adminIndex.html"><i class="fas fa-user-cog"></i></a>'

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    document.location.reload();
  }
}
