// 사용자 리스트 추가하기
function addQnAList(id, title, state, userName, questionDate, answerDate, index) {
  var qnaRow = '<div class="row tbodyRow" onClick="linkQnADetail(' + id + ')">';
    qnaRow += '<div class="cell">' + index + '</div>',
    qnaRow += '<div class="cell">' + title + '</div>',
    qnaRow += '<div class="cell">' + userName + '</div>',
    qnaRow += '<div class="cell">' + state + '</div>',
    qnaRow += '<div class="cell">' + questionDate + '</div>',
    qnaRow += '<div class="cell">' + answerDate + '</div>',
    qnaRow += '</div>'

  $('.tbody').append(qnaRow);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  var selectVal = $('#qnaList_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $(document).find('strong').text();
  var title = "";
  var state = "";
  var userName = "";


  if (selectVal == 'title') {
    title = inputVal
  }
  if (selectVal == 'state') {
    state = inputVal
  }
  if (selectVal == 'userName') {
    content = inputVal
  }

  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();
    document.location.href = 'mainQnAList.html?inputVal=' + inputVal + '&selectVal=' + selectVal + '&pageNo=' + pageNo;
  }
}
