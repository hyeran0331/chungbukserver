//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    document.location.reload();
  }
}
