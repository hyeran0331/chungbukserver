//menu에 userinfo 보여주기
function addMenuInfo(name, department, email) {
  var menuInfo = '<span class="username" id="userName">' + name + '</span>';
  menuInfo += '<span class="usercompany" id="userCompany">' + department + '</span>';
  menuInfo += '<span class="useremail blind" id="userEmail">' + email + '</span>';
  $('.info_text').append(menuInfo);
}

function addBoardType(typeId, title, description){
  var boardType = '<a href="boardList.html?typeId='+typeId+'&title='+title+'" class="pageLink">'+title+'</a>';
  $('.board_list').append(boardType);
}

//iframe reload
document.onkeydown = trapRefresh;

function trapRefresh() {
  if (event.keyCode == 116 || (event.ctrlKey == true && event.keyCode == 82) || (event.metaKey == true && event.keyCode == 82)) {
    event.keyCode = 0;
    event.cancelBubble = true;
    event.returnValue = false;
    document.iframeContents.location.reload();
  }
}
