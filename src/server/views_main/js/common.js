// 서버 url
// serverURL = 'http://holichsoft.cafe24.com:8084';
// serverURL = 'https://appdispenser.com';
// serverURL = 'http://1.234.5.157:8081';
serverURL = 'http://localhost:8083';
// serverURL = 'http://192.168.0.20:8083';


// setUserToken function
function setUserToken(token) {
  // console.log("setUserToken = " + token)
  setCookie('userToken', token, 3)
}

function getUserToken() {
  let userToken = getCookie('userToken')
  // console.log('getUserToken = ' + userToken)
  return userToken
}

// setCookie function
function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

// getCookie function
function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

// getXMLHttpRequest function
function getXMLHttpRequest() {
  //브라우저가 IE일 경우 XMLHttpRequest 객체 구하기
  if (window.ActiveXObject) {
    //Msxml2.XMLHTTP가 신버전이어서 먼저 시도
    try {
      return new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        return new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        return null;
      }
    }
    //IE 외 파이어폭스 오페라 같은 브라우저에서 XMLHttpRequest 객체 구하기
  } else if (window.XMLHttpRequest) {
    return new XMLHttpRequest();
  } else {
    return null;
  }
}
// getQueryStringObject function
function getQueryStringObject() {
  var a = window.location.search.substr(1).split('&');
  if (a == "") return {};
  var b = {};
  for (var i = 0; i < a.length; ++i) {
    var p = a[i].split('=', 2);
    if (p.length == 1)
      b[p[0]] = "";
    else
      b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
  }
  return b;
}

//date function
var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

function yyyymmdd(date) {
  var x = date;
  var y = x.getFullYear().toString();
  var m = (x.getMonth() + 1).toString();
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var yyyymmdd = y + m + d;
  return yyyymmdd;
}

function yyyy_mm_dd(date) {
  var x = date;
  var y = x.getFullYear().toString();
  var m = (x.getMonth() + 1).toString();
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var yyyymmdd = y + "-" + m + "-" + d;
  return yyyymmdd;
}

function mm_ddyyyy(date) {
  var x = date;
  var y = x.getFullYear().toString();
  var m = month[x.getMonth()];
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var mmddyyyy = m + " " + d + ", " + y;
  return mmddyyyy;
}

function mm_dd(date) {
  var x = date;
  var m = month[x.getMonth()];
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var mmdd = m + " " + d;
  return mmdd;
}

function yyyymmddDot(date) {
  var x = date;
  var y = x.getFullYear().toString();
  var m = (x.getMonth() + 1).toString();
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var yyyymmdd = y + "." + m + "." + d;
  return yyyymmdd;
}

function hhmmss(date) {
  var x = date;
  var h = x.getHours().toString();
  var m = x.getMinutes().toString();
  var s = x.getSeconds().toString();
  (h.length == 1) && (h = '0' + h);
  (s.length == 1) && (s = '0' + s);
  (m.length == 1) && (m = '0' + m);
  var hhmmss = h + m + s;
  return hhmmss;
}

function hh_mm_ss(date) {
  var x = date;
  var h = x.getHours().toString();
  var m = x.getMinutes().toString();
  var s = x.getSeconds().toString();
  (h.length == 1) && (h = '0' + h);
  (s.length == 1) && (s = '0' + s);
  (m.length == 1) && (m = '0' + m);
  var hhmmss = h + ":" + m + ":" + s;
  return hhmmss;
}

function hh_mm(date) {
  var x = date;
  var h = x.getHours().toString();
  var m = x.getMinutes().toString();
  (h.length == 1) && (h = '0' + h);
  (m.length == 1) && (m = '0' + m);
  var hhmm = h + ":" + m;
  return hhmm;
}

function mm_ddyyyyMonth(time) {
  let date = new Date(time);
  var x = date;
  var y = x.getFullYear().toString();
  var m = month[x.getMonth()];
  var d = x.getDate().toString();
  (d.length == 1) && (d = '0' + d);
  (m.length == 1) && (m = '0' + m);
  var mmddyyyy = m + " " + d + ", " + y;
  return mmddyyyy;
}

function yyyymmdd_hhmmss(time) {
  let date = new Date(time);
  let dayString = yyyymmdd(date);
  let timeString = hhmmss(date);
  return dayString + " " + timeString;
}

function yyyy_mm_dd_hh_mm_ss(time) {
  let date = new Date(time);
  let dayString = yyyy_mm_dd(date);
  let timeString = hh_mm_ss(date);
  return dayString + " " + timeString;
}

function yyyy_mm_dd_hh_mm(time) {
  let date = new Date(time);
  let dayString = yyyy_mm_dd(date);
  let timeString = hh_mm(date);
  return dayString + " " + timeString;
}

function yyyymmddDot_hh_mm(time) {
  let date = new Date(time);
  let dayString = yyyymmddDot(date);
  let timeString = hh_mm(date);
  return dayString + " " + timeString;
}

function yyyymmddDot_hh_mm_ss(time) {
  let date = new Date(time);
  let dayString = yyyymmddDot(date);
  let timeString = hh_mm_ss(date);
  return dayString + " " + timeString;
}

function yyyymmddD(time) {
  let date = new Date(time);
  let dayString = yyyymmddDot(date);
  return dayString
}

// list 빈화면
var empty = '<div class="emptyUserList"><span>This list is empty</span></div>'
//errorMessage
var modalMainBody = $('#modalMain', window.parent.document);
var errorMessageHtml = '<div id="modalMainMask" class="mask" role="dialog"></div><div class="errorMessage" role="alert"><div class="warningImg"><img src="../images/warning.svg" alt="warning"></div><p class="errorText"></p><div class="buttonBox"><button type="button" class="halfWidthBtn cancel">Cancel</button><button type="button" class="halfWidthBtn ok">OK</button></div></div>'
var errorMessageOneBtnHtml = '<div id="modalMainMask" class="mask" role="dialog"></div><div class="errorMessage" role="alert"><div class="warningImg"><img src="../images/warning.svg" alt="warning"></div><p class="errorText"></p><div class="buttonBox"><button type="button" class="okBtn ok">OK</button></div></div>'
// deleteMessage
var deleteMessage = 'Are you sure you want to delete the selected information?'
var noDeleteMessage = 'No select.<br>Please select a list to delete.'
var adminCantDeleteMessage = 'You cannot delete ADMIN.<br>Please deselect it.'
var cantReleaseMessage = 'You cannot release this member.<br>Please deselect it.'
var withdrawaMessage = 'Would you like to withdraw?'
var releaseMessage = 'Are you sure you want to release the selected information?'
var noReleaseMessage = 'No select.<br>Please select a list to release.'
var deleteQnAMessage = 'Are you sure you want to delete the QnA?'
var changeGradeMessage = 'Are you sure you want to changed grade the selected information?'
var noChangeGradeMessage = 'No select.<br>Please select a list to change grade.'
var deleteBoardMessage = 'Are you sure you want to delete this board?'


// loadingMessage
var qnaLoadingHtml = '<div id="modalMainMask" class="mask" role="dialog"></div><div class="questionLoading"><div><img src="images/icon_rowLoading.svg" alt="loading"><span>loading...</span></div></div>';
// cancelMessage
var cancelMessage = 'If you cancel, the content will not save. Are you sure you want to cancel?'

// popup 버튼 클릭
// cancel 버튼 클릭
function popupBtnCancel() {
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
}

// deleteMessagePopup
function deleteMessagePopup(confirmId) {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(deleteMessage);
  // cancel 버튼 클릭
  popupBtnCancel();
  // ok 버튼 클릭
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    deleteList(confirmId);
  });
}

// deleteBoardMessagePopup
function deleteBoardMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(deleteBoardMessage);
  // cancel 버튼 클릭
  popupBtnCancel();
  // ok 버튼 클릭
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    deleteBoard();
  });
}

// ChangeGradeMessagePopup
function changeGradeMessagePopup(userId) {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(changeGradeMessage);
  // cancel 버튼 클릭
  popupBtnCancel();
  // ok 버튼 클릭
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    changeGradeList(userId);
  });
}


function withdrawUser(target) {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(withdrawaMessage);
  // cancel 버튼 클릭
  popupBtnCancel();
  // ok 버튼 클릭
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    deleteAdmin(target.attr('data-value'))
  });
}

// cantDeleteMessagePopup
function cantDeleteList(errorMessage) {
  modalMainBody.prepend(errorMessageOneBtnHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(errorMessage)
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
}

// windowRefresh
function windowRefresh(errorText) {
  modalMainBody.prepend(errorMessageOneBtnHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(errorText);
  //errorMessage button click
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    parent.location.href = 'adminMain.html'
  });
}

// success false reasoncode
function FalseOfDataSuccess(reasonCode, failMessage) {
  $(document).ready(function() {
    if (reasonCode == 1) {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html('Session out.<br>Go to the login page.');
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = 'signIn.html';
      });
    } else if (reasonCode == 2) {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html('The user changed.<br>Window will be refresh.');
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = 'index.html';
      });
    } else {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html(failMessage);
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = 'index.html';
      });
    }
  })
}

// success false reasoncode
function adminFalseOfDataSuccess(reasonCode, failMessage) {
  $(document).ready(function() {
    if (reasonCode == 1) {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html('Session out.<br>Go to the login page.');
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = '../index.html';
      });
    } else if (reasonCode == 2) {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html('The user changed.<br>Window will be refresh.');
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = 'adminIndex.html';
      });
    } else {
      modalMainBody.find('.errorMessage').remove();
      modalMainBody.append(errorMessageOneBtnHtml);
      modalMainBody.find('.errorText').html(failMessage);
      modalMainBody.find('.ok').click(function() {
        modalMainBody.find('#modalMainMask').fadeOut(function() {
          $(this).remove();
        });
        modalMainBody.find('.errorMessage').fadeOut(function() {
          $(this).remove();
        });
        parent.location.href = 'adminIndex.html';
      });
    }
  })
}
