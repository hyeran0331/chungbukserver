// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

$(document).ready(function() {
  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })
})

function enterkey() {
  if (window.event.keyCode == 13) {
    signInUser();
  }
}

function signInUser() {
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText signInUser= " + httpRequest.responseText);
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var email = data.session;
          setUserToken(email)
          window.location.href = 'index.html';
        } else {
          alert(data.reason)
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.send(JSON.stringify({
    'email': $('#email').val(),
    'password': $('#password').val()
  }));
}
