// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  getUserSession();

  $('.qna').click(function() {
    $('.qna_list').slideToggle();
  })
  $('.board').click(function() {
    $('.board_list').slideToggle();
  })
})

//menu Link 이동
function menuLink(target) {
  event.preventDefault();
  var thisLink = target.attr('href');
  var iframe = $('iframe');
  var iframeSrc = iframe.attr('src');
  var srcChange = iframeSrc.replace(iframeSrc, thisLink);

  target.parent().siblings().removeClass('on');
  target.parent().addClass('on');
  iframe.attr('src', srcChange);
}

// 세션 불러오기
function getUserSession() {
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getUserSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var email = data.session;
          var userName = data.results.userName;
          var affiliation = data.results.affiliation;
          var duty = data.results.duty;
          setUserToken(email)
          addMenuInfo(userName, affiliation, email)
        } else {
          FalseOfDataSuccess(data.reasonCode, data.reason)
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

function getBoardType(){
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getBoardType= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          for (var i = 0; i < data.results.rows.length; i++) {
            var typeId = data.results.rows[i].id;
            var title = data.results.rows[i].title;
            var description = data.results.rows[i].description;

            addBoardType(typeId, title, description)

            $('.pageLink').click(function(){
              menuLink($(this))
            })
          }
        } else {
          FalseOfDataSuccess(data.reasonCode, data.reason)
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/type/?content=&offset=0&limit=100', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

function checkType(type, count){
  console.log("type = " + type);
  console.log("count = " + count);
}

// 로그아웃하기
function logoutUser() {
  event.preventDefault();
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText = " + httpRequest.responseText);
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          window.location.href = 'index.html'
        } else {
          alert("user logout fail");
          window.location.href = 'index.html'
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
