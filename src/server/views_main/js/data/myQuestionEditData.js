// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var inputFileChage = '';

$(document).ready(function() {
  //summernote 부분
  $('#summernote').summernote({
    height: 250,
    minHeight: null,
    maxHeight: 600,
    focus: true,
    lang: 'ko-KR',
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'paragraph']],
    ]
  });

  getQnADetail()

  // 유효성 검사
  $('#question_title').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('.note-editable').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

});

// 버튼 disabled 부여
function updateDisable() {
  $('.ok_btn').removeClass('on');
  $('.ok_btn').attr('disabled', true);
}

// title 컨펌
function confirmTitle() {
  updateDisable()
  // validation check
  if ($('#question_title').val() == '') {
    return false;
  } else {
    return true;
  }
}
// content 컨펌
function confirmContent() {
  updateDisable()
  // validation check
  if ($('.note-editable').text() == '') {
    return false;
  } else {
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmTitle()) {
    return;
  }
  if (!confirmContent()) {
    return;
  }
  $('.ok_btn').addClass('on');
  $('.ok_btn').attr('disabled', false);
}

// 취소 버튼
function gotoDetail() {
  document.location.href = 'mainQnADetail.html?qnaId=' + queryString.qnaId + '&qnaList=' + queryString.qnaList + '&selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
}

// 리스트의 처음 pagination으로 이동
function linkPage() {
  document.location.href = 'myQnAList.html?selectVal=' + queryString.selectVal + '&pageNo=' + 1;
}

// cancel 버튼 모달
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoDetail()
  });
}

//세션 확인하기
function getQnADetail() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getQnADetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var title = data.results.title;
          var question = data.results.title;

          $('#question_title').val(title);
          $('.note-editable').html(question);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get Question Detail.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/qna/' + queryString.qnaId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//  추가
function editQustion() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText addQustion= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          linkPage()
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to add Question.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/qna/question/' + queryString.qnaId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'title': $('#question_title').val(),
    'state': '문의요청',
    'question': $('.note-editable').html()
  }));
}
