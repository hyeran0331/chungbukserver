// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  $('.link_form').append(signLink)

  confirmSession()

  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })
})

function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmUserSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          $('.link_form').empty();
          $('.link_form').append(mainLink)

          var duty = data.results.duty;
          if (duty == "ADMIN") {
            $('.admin_link').append(adminLink)
          }
        } else {
          $('.link_form').empty();
          $('.link_form').append(signLink)
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 로그아웃하기
function userLogout() {
  event.preventDefault();
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText = " + httpRequest.responseText);
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          window.location.href = 'index.html'
        } else {
          alert("user logout fail")
          window.location.href = 'index.html'
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
