// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
// 비밀번호 정규식
var passwordRegExp = RegExp(/^(?=.*[A-Za-z])(?=.*\d)[\w!@#$%^&*()+={}]{8,}$/);
var mailRegExp = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
var nameRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var departmentRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var spaceRegExp = RegExp(/^[\s]+$/);
var phoneNumberRegExp = RegExp(/^\d{3}-\d{3,4}-\d{4}$/);


// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  //form 유효성 체크
  $('#email').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#backup_email').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#password').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#con_password').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#name').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#phone_number').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#affiliation').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#major').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#position').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
})

// 버튼 disabled 부여
function updateDisable() {
  $('.sign_up_btn').removeClass('on');
  $('.sign_up_btn').attr('disabled', true);
}

// 비밀번호 컨펌
function confirmPassword() {
  updateDisable()
  // validation check
  if (!passwordRegExp.test($('#password').val())) {
    if ($('#password').val() == '') {
      $('#password').attr('placeholder', '* Input your Password');
      $('.password_form').addClass('on_placeholder')
    } else {
      $('.password_form').find('p').html("* Please enter at least 8 characters combining English and numbers.")
    }
    return false;
  } else {
    $('.password_form').find('p').empty();
    return true;
  }
}

// 이중 비밀번호 컨펌
function confirmConPassword() {
  updateDisable()
  // validation check
  if ($('#password').val() != $('#con_password').val()) {
    $('.con_password_form').find('p').html("* Passwords do not match.")
    return false;
  } else {
    $('.con_password_form').find('p').empty();
    return true;
  }
}

// 이름 컨펌
function confirmName() {
  updateDisable()
  // validation check
  if ($('#name').val() == '' || spaceRegExp.test($('#name').val()) == true) {
    $('#name').attr('placeholder', "* Please enter a name.");
    $('.name_form').addClass('on_placeholder')
  } else if (!nameRegExp.test($('#name').val())) {
    $('#name').attr('placeholder', "* Hangul and English or Please enter using '. _ -'.");
    $('.name_form').addClass('on_placeholder')
    return false;
  } else {
    $('#name').attr('placeholder', '');
    return true;
  }
}

// 이름 컨펌
function confirmPhoneNumber() {
  updateDisable()
  // validation check
  if ($('#phone_number').val() == '' || spaceRegExp.test($('#name').val()) == true) {
    $('#phone_number').attr('placeholder', "* Please enter a phone number.");
    $('.phone_number_form').addClass('on_placeholder')
  } else if (!phoneNumberRegExp.test($('#phone_number').val())) {
    $('#phone_number').attr('placeholder', "* Please enter it according to the phone number format.");
    $('.phone_number_form').addClass('on_placeholder')
    return false;
  } else {
    $('#phone_number').attr('placeholder', '');
    return true;
  }
}

// 메일 컨펌
function confirmEmail() {
  updateDisable()
  // validation check
  if ($('#email').val() == '') {
    $('#email').attr('placeholder', "* Please enter user email.");
    $('.email_form').addClass('on_placeholder')
    return false;
  } else if (!mailRegExp.test($('#email').val())) {
    $('#email').attr('placeholder', "* Please enter it according to the email format.");
    $('.email_form').addClass('on_placeholder')
    return false;
  } else {
    $('#email').attr('placeholder', '');
    return true;
  }
}
// 메일 컨펌
function confirmBackupEmail() {
  updateDisable()
  // validation check
  if ($('#backup_email').val() == '') {
    $('#backup_email').attr('placeholder', "* Please enter user backup email.");
    $('.backup_email_form').addClass('on_placeholder')
    return false;
  } else if (!mailRegExp.test($('#backup_email').val())) {
    $('#backup_email').attr('placeholder', "* Please enter it according to the email format.");
    $('.backup_email_form').addClass('on_placeholder')
    return false;
  } else {
    $('#backup_email').attr('placeholder', '');
    return true;
  }
}

// 직장 컨펌
function confirmAffiliation() {
  updateDisable()
  // validation check
  if ($('#affiliation').val() == '' || spaceRegExp.test($('#affiliation').val()) == true) {
    $('#affiliation').attr('placeholder', "* Please enter a affiliation.");
    $('.affiliation_form').addClass('on_placeholder')
  } else if (!departmentRegExp.test($('#affiliation').val())) {
    $('#affiliation').attr('placeholder', "* Hangul and English or Please enter using '. _ -'.");
    $('.affiliation_form').addClass('on_placeholder')
    return false;
  } else {
    $('#affiliation').attr('placeholder', '');
    return true;
  }
}
// 부서 컨펌
function confirMajor() {
  updateDisable()
  // validation check
  if ($('#major').val() == '' || spaceRegExp.test($('#major').val()) == true) {
    $('#major').attr('placeholder', "* Please enter a major.");
    $('.major_form').addClass('on_placeholder')
  } else if (!departmentRegExp.test($('#major').val())) {
    $('#major').attr('placeholder', "* Hangul and English or Please enter using '. _ -'.");
    $('.major_form').addClass('on_placeholder')
    return false;
  } else {
    $('#major').attr('placeholder', '');
    return true;
  }
}
// 직책 컨펌
function confirmPosition() {
  updateDisable()
  // validation check
  if ($('#position').val() == '' || spaceRegExp.test($('#position').val()) == true) {
    $('#position').attr('placeholder', "* Please enter a position.");
    $('.position_form').addClass('on_placeholder')
  } else if (!departmentRegExp.test($('#position').val())) {
    $('#position').attr('placeholder', "* Hangul and English or Please enter using '. _ -'.");
    $('.position_form').addClass('on_placeholder')
    return false;
  } else {
    $('#position').attr('placeholder', '');
    return true;
  }
}


// 유효성 체크
function checkValidation() {
  if (!confirmEmail()) {
    return;
  }
  if (!confirmBackupEmail()) {
    return;
  }
  if (!confirmPassword()) {
    return;
  }
  if (!confirmConPassword()) {
    return;
  }
  if (!confirmName()) {
    return;
  }
  if (!confirmPhoneNumber()) {
    return;
  }
  if (!confirmAffiliation()) {
    return;
  }
  if (!confirMajor()) {
    return;
  }
  if (!confirmPosition()) {
    return;
  }
  // 유효성 검사를 통과하면 사용자를 추가한다.
  $('.sign_up_btn').addClass('on');
  $('.sign_up_btn').attr('disabled', false);
}

// 사용자를 추가
function signUp() {
  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').removeClass('blind');
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText addUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })
          alert("회원가입에 성공했습니다.")
          document.location.href = 'index.html';
        } else {
          alert(data.reason)
          document.location.reload();
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/api/user/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    "email": $('#email').val(),
    "backupEmail": $('#backup_email').val(),
    "password": $('#password').val(),
    "userName": $('#name').val(),
    "gender": $('input[name="gender"]:checked').val(),
    "phoneNumber": $('#phone_number').val(),
    "generalNumber": $('#general_number').val(),
    "job": $('#job').val(),
    "area": $('#area').val(),
    "affiliation": $('#affiliation').val(),
    "major": $('#major').val(),
    "position": $('#position').val(),
    "comTelephone": $('#com_telephone').val(),
    "comFax": $('#com_fax').val(),
    "postalCode": $('#postal_code').val(),
    "address": $('#address').val(),
    "detailAddress": $('#detail_address').val(),
    "grade": "임시회원",
    "duty": "USER"
  }));
}
