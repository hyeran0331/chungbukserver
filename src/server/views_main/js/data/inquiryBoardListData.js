// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  $('.title').text(queryString.title)
  // select 값이 변하면 변한 값에 맞게 input 속성을 수정한다.
  $("#freeboard_select").on('change', function() {
    var selectVal = $(this).val();

    function selectOn() {
      $('#search_input').attr('readonly', false)
      $('#search_input').attr('placeholder', '검색어를 입력해주세요.')
    }
    if (selectVal == 'title') {
      selectOn();
    } else if (selectVal == 'content') {
      selectOn();
    } else if (selectVal == '0') {
      $('#search_input').val('');
      $('#search_input').attr('readonly', true)
      $('#search_input').attr('placeholder', '')
      $('#inquiryboard_select').val('0');
      $('#freeboard_select').val('0');
    }
  })

  // delete click시 체크유무 판단후 popup띄우기
  $('#deleteBtn').click(function() {
    if ($('input[name="eachCheck"]').is(':checked') == true) {
      deleteMessagePopup()
    } else if ($('input[name="eachCheck"]').is(':checked') == false) {
      cantDeleteList(noDeleteMessage)
    }
  })

  if (JSON.stringify(queryString) === JSON.stringify({})) {
    getPageList(1)
  } else {
    var select = $('#freeboard_select');
    var input = $('#search_input');
    select.val(queryString.selectVal);
    input.val(queryString.inputVal);

    // reload 시 select val 에 따라 input 의 속성을 수정한다.
    if (select.val() == '0') {
      $('#search_input').attr('readonly', true)
    } else {
      $('#search_input').attr('readonly', false)
    }

    // empty list 에서 reload시 pageNo 값을 정의 한다.
    if (queryString.pageNo == '') {
      getSearchList(1)
    } else {
      getSearchList(queryString.pageNo)
    }
  }

});


// 추가 버튼
function addBtn() {
  var selectVal = $('#freeboard_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $('.pagination').find('strong').text();
  var boardType = 'freeBoard'

  document.location.href = 'boardAdd.html?selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo + '&boardType=' + boardType;
}

//모든 체크박스를 선택한다.
function allCheck() {
  function checkboxOn() {
    $('.checkboxImg').attr('src', 'images/checkbox_on.svg')
  }

  function checkboxOff() {
    $('.checkboxImg').attr('src', 'images/checkbox_off.svg')
  }
  if ($('#allCheck').is(':checked') == true) {
    $('input[name="eachCheck"]').prop('checked', true);
    checkboxOn()
  } else {
    $('input[name="eachCheck"]').prop('checked', false);
    checkboxOff()
  }
}

// 해당 목록을 삭제한다.
var removeView = [];
var removeList = [];

// 다중리스트 삭제
function deleteList() {
  $('input[name="eachCheck"]:checked').each(function() {
    removeList.push($(this).attr('id'));
  });
  if (removeList.length > 0) {
    deleteOne(removeList[0]);
  }
}

// 리스트 하나 삭제
function deleteOne(boardId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText deleteOne= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          removeList.splice(0, 1);
          if (removeList.length > 0) {
            deleteOne(removeList[0]);
          } else {
            for (var i = 0; i < removeView.length; i++) {
              $('input#' + removeView[0]).parent().parent().remove();
              removeView.splice(0, 1);
            }
            // 체크박스 해제
            if ($('#allCheck').is(':checked') == true) {
              $('#allCheck').prop('checked', false);
            }
            // 새로 목록 가져오기
            var pageNum = $(document).find('strong').text();
            getSearchList(pageNum);
            $('.delete_btn').blur();
          }
        } else {
          for (var i = 0; i < removeView.length; i++) {
            $('input#' + removeView[0]).parent().parent().remove();
            removeView.splice(0, 1);
          }
          // 새로 목록 가져오기
          var pageNum = $(document).find('strong').text();
          getSearchList(pageNum);
          $('.delete_btn').blur();
          // 체크박스 해제
          if ($('#allCheck').is(':checked') == true) {
            $('#allCheck').prop('checked', false);
          }
          FalseOfDataSuccess(data.reasonCode, 'Failed to Delete');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/board/' + boardId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 체크박스 클릭
function labelClick(event, target) {
  event.stopImmediatePropagation();
}

function inputClick(event, target) {
  event.stopImmediatePropagation();
  target.each(function() {
    if ($(this).is(':checked') == false) {
      $('#allCheck').prop('checked', false);
    }
  });
}

// popup 정보 이동
function linkBoardDetail(id) {
  var selectVal = $('#freeboard_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $('.pagination').find('strong').text();
  var boardType = 'freeBoard';

  document.location.href = 'boardDetail.html?boardId=' + id + '&selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo + '&boardType=' + boardType;
}

// 리스트 목록 가져오기
function getPageList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getPageList= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = Paging(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var boardListInfo = data.results.rows[i];
              var id = boardListInfo.id;
              var type = boardListInfo.type.title;
              var title = boardListInfo.title;
              var content = boardListInfo.content;
              var date = yyyymmddDot_hh_mm_ss(boardListInfo.created_at);

              var postUser = boardListInfo.postUser;
              var userName = boardListInfo.user.email;

              addBoardList(id, type, title, userName, date, index)
            }
          } else {
            $('.tbody').empty();
            $('.tbody').append(empty);
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get the corresponding page list.')
        }

      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }

  httpRequest.open('get', serverURL + '/api/board/?type=inquiryboard&title=&content=&offset=' + 10 * (pageNo - 1) + '&limit=' + 10, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
Paging = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getPageList(' + s2 + ')>');
    html.push('<img src="images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');
    } else {
      html.push('<a href=javascript:getPageList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getPageList(' + nextPageNum + ')>');
    html.push('<img src="images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}

// 검색 엔터키
function enterkey() {
  if (window.event.keyCode == 13) {
    search();
  }
}

// 검색하기
function search() {
  var selectVal = $('#freeboard_select').val();
  var inputVal = $('#search_input').val();
  getSearchList(1);
}

// search url
function makeSearchUrl(offsetNum) {
  var selectVal = $('#freeboard_select').val();
  var inputVal = $('#search_input').val();
  var title = ""
  var content = ""

  if (selectVal == 'title') {
    title = inputVal
  }
  if (selectVal == 'content') {
    content = inputVal
  }

  var getListUrl = '/api/board/?type=inquiryboard&title=' + title + '&content=' + content + '&offset=' + offsetNum + '&limit=' + 10;
  return getListUrl
}

// 검색된 리스트
function getSearchList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getSearchList= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = PagingSearch(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var boardListInfo = data.results.rows[i];
              var id = boardListInfo.id;
              var type = boardListInfo.type.title;
              var title = boardListInfo.title;
              var content = boardListInfo.content;
              var date = yyyymmddDot_hh_mm_ss(boardListInfo.created_at);

              var postUser = boardListInfo.postUser;
              var userName = boardListInfo.user.email;

              addBoardList(id, type, title, userName, date, index)
            }
          } else {
            if (pageNo == '1') {
              $('.tbody').empty();
              $('.tbody').append(empty);
            } else {
              getSearchList(pageNo - 1)
            }
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          //전체 페이지 로딩 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get list of search pages.');
        }

      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  let searchUrl = makeSearchUrl((pageNo - 1) * 10)
  httpRequest.open('get', serverURL + searchUrl, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
PagingSearch = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getSearchList(' + s2 + ')>');
    html.push('<img src="images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');

    } else {
      html.push('<a href=javascript:getSearchList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getSearchList(' + nextPageNum + ')>');
    html.push('<img src="images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}
