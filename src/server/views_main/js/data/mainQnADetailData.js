// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  confirmSession()
})

function gotoList() {
  document.location.href = '' + queryString.qnaList + '.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
}

function editBtn(qnaId) {
  document.location.href = 'myQuestionEdit.html?qnaId=' + qnaId + '&qnaList=' + queryString.qnaList + '&selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
}

//세션 확인하기
function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var userId = data.results.id;
          getQnADetail(userId);

        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//  데이터 불러오기
function getQnADetail(userId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getQnADetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var qnaId = data.results.id;
          var title = data.results.title;
          var state = data.results.state;
          var questionUserId = data.results.questionUserId;
          var questionDate = yyyymmddDot_hh_mm_ss(data.results.questionDate);

          if (userId == questionUserId) {
            $('.edit_btn').removeClass("hidden");
          }

          $('.info_title').find('span').html(title);
          $('.info_date').find('span').html(questionDate);
          $('.info_state').find('span').html(state);

          var questionContent = data.results.question;
          var userName = data.results.user.userName;
          var userEmail = data.results.user.email;
          var userNumber = data.results.user.phoneNumber;

          $('.question_content').find('span').html(questionContent);
          $('.user_info_container').find('.user_email').html(userEmail);

          var answerContent = data.results.answer;
          if (answerContent == null) {
            $('.answer_container').addClass('hidden');
          } else {
            $('.answer_content').find('span').html(answerContent);
          }

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

          // ok click event
          $('.edit_btn').click(function() {
            editBtn(qnaId)
          })

        } else {
          FalseOfDataSuccess(data.reasonCode, data.reason)
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/qna/' + queryString.qnaId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
