// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var userId;
var userDuty;
var postUser;

// 세션을 확인한다.
$(document).ready(function() {
  // console.log(queryString);

  confirmSession()

  //summernote 부분
  $('#summernote').summernote({
    height: 40,
    minHeight: null,
    maxHeight: 100,
    focus: true,
    lang: 'ko-KR',
    placeholder: 'Please enter the content.',
    toolbar: null,
  });
})

// 취소 버튼
function gotoList() {
  history.back()
  // document.location.href = 'boardList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo +'&typeId=' + queryString.typeId +'&title=' + queryString.boardType
}

function getfileSize(size) {
  var s = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'];
  var e = Math.floor(Math.log(size) / Math.log(1024));
  return (size / Math.pow(1024, e)).toFixed(2) + " " + s[e];
};

// cancel 버튼 모달
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}

//세션 확인하기
function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var userId = data.results.id;
          var userDuty = data.results.duty;
          getBoardDetail(userId, userDuty)

        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// detail data 불러오기
function getBoardDetail(userId, userDuty) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getBoardDetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // board info 불러와서 보여주기
          var boardId = data.results.id;
          boardType = data.results.typeId;
          boardTitle = data.results.title;
          boardContent = data.results.content;
          boardCreatedAtDot = yyyymmddDot_hh_mm_ss(data.results.created_at);
          boardUserName = data.results.user.email;
          postUser = data.results.postUser;

          $('.board_title').find('span').text(boardTitle);
          $('.board_writer').find('span').text(boardUserName);
          $('.board_content').find('span').html(boardContent);
          $('.board_date').find('span').text(boardCreatedAtDot);

          // file info 불러와서 보여주기
          for (var i = 0; i < data.results.file.length; i++) {
            var fileInfo = data.results.file[i];
            var id = fileInfo.id;
            var fieldname = fileInfo.fieldname;
            var originalname = fileInfo.originalname;
            var encoding = fileInfo.encoding;
            var mimetype = fileInfo.mimetype;
            var destination = fileInfo.destination;
            var filename = fileInfo.filename;

            var size = fileInfo.size;
            var fixedSize = getfileSize(size)

            var path = fileInfo.path;
            var fixedPath = '/uploads/' + fieldname + "/";
            var downloadPath = fixedPath + filename

            $('.file_data').empty()

            // file의 mimetype에 따라 보여주기
            if (mimetype.includes('image')) {
              $('.file_data').append(imageCon)
              $('.file_data').find('img').attr('src', downloadPath)
            } else if (mimetype.includes('video') || mimetype.includes('audio')) {
              $('.file_data').append(videoCon)
              $('.file_data').find('video').attr('src', downloadPath)
            }
            addFileInfo(originalname, fixedSize, mimetype, downloadPath)
          }

          if (userId == postUser || userDuty == "ADMIN") {
            $('.delete_btn').removeClass('hidden');
          }

          $('.user_cbox_upload_btn').click(function() {
            addComment(boardId, boardType)
          })

          // comment info 불러와 보여주기
          var parentBoardLength = data.results.parentBoard.length
          var commentCount = parentBoardLength;

          for (var j = 0; j < data.results.parentBoard.length; j++) {
            var parentBoardInfo = data.results.parentBoard[j];
            var id = parentBoardInfo.id;
            var type = parentBoardInfo.typeId;
            var title = parentBoardInfo.title;
            var content = parentBoardInfo.content;
            var postUser = parentBoardInfo.postUser;
            var boardId = parentBoardInfo.boardId;
            var parentId = parentBoardInfo.parentId;
            var date = yyyymmddDot_hh_mm_ss(parentBoardInfo.created_at);
            var userName = parentBoardInfo.user.userName;
            var userEmail = parentBoardInfo.user.email;

            addCommentInfo(id, type, title, content, postUser, boardId, parentId, date, userName, userEmail)

            if (userId == postUser || userDuty == "ADMIN") {
              $('.delete_commnet_btn_'+id+'').removeClass('hidden');
            }

            // reply info 불러와 보여주기
            var childBoardLength = data.results.parentBoard[j].childBoard.length;
            commentCount = commentCount + childBoardLength;
            for (var k = 0; k < data.results.parentBoard[j].childBoard.length; k++) {
              var childBoardInfo = parentBoardInfo.childBoard[k];
              var id = childBoardInfo.id;
              var type = childBoardInfo.typeId;
              var title = childBoardInfo.title;
              var content = childBoardInfo.content;
              var postUser = childBoardInfo.postUser;
              var boardId = childBoardInfo.boardId;
              var parentId = childBoardInfo.parentId;
              var date = yyyymmddDot_hh_mm_ss(childBoardInfo.created_at);
              var userName = childBoardInfo.user.userName;
              var userEmail = childBoardInfo.user.email;
              addReplyInfo(id, type, title, content, postUser, boardId, parentId, date, userName, userEmail)

              if (userId == postUser || userDuty == "ADMIN") {
                $('.delete_reply_btn_'+id+'').removeClass('hidden');
              }
            }

          }
          $('.user_cbox_head').find('span').text(commentCount);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut(function() {
            $(this).remove();
          });

        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to get Board Detail.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/board/' + queryString.boardId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//  추가
function addComment(id, boardType) {
  console.log("addComment boardType = " + boardType);
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText addComment= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          document.location.reload();
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to add Board.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/api/board/comments', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'typeId': boardType,
    'content': $('.note-editable').html(),
    'boardId': queryString.boardId,
    'parentId': id
  }));
}

//  추가
function addReply(id, boardType) {
  console.log("addReply boardType = " + boardType);
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText addComment= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          document.location.reload();
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to add Board.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/api/board/comments', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'typeId': boardType,
    'content': $('.user_cbox_reply_write').find('.note-editable').html(),
    'boardId': queryString.boardId,
    'parentId': id
  }));
}

// board delete
function deleteBoard() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText deleteBoard= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          gotoList();
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to Delete Board.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/board/' + queryString.boardId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
// board delete
function deleteComment(id) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText deleteComment= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          document.location.reload();
        } else {
          FalseOfDataSuccess(data.reasonCode, 'Failed to Delete Board.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/board/' + id, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
