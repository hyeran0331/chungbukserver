// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {

  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })

  // 유효성 검사
  $('#type').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#title').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#target').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#popupStartDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#popupEndDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#display_left').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#display_top').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#image_file').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#size').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#exposure').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

  // 프로필 사진 file 변수(data에서 프로필사진 변경시 필요)
  var files;
  //profile image change
  var readURL = function(input) {
    if (input.files && input.files[0]) {
      if (input.files[0].size < 10 * 1024 * 1000) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image_form').find('img').attr('src', e.target.result)
          $('.image_form').find('img').attr('alt', 'popup image')
          dataURL = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        alert("error")
      }
    }
  }
  $('#image_file').on('change', function(file) {
    readURL(this);
    files = file.target.files[0];
  });

})

// 버튼 disabled 부여
function updateDisable() {
  $('.add_btn').removeClass('on');
  $('.add_btn').attr('disabled', true);
}

// type 컨펌
function confirmType() {
  updateDisable()
  // validation check
  if ($('#type').val() == '') {
    return false;
  } else {
    return true;
  }
}
// title 컨펌
function confirmTitle() {
  updateDisable()
  // validation check
  if ($('#title').val() == '') {
    return false;
  } else {
    return true;
  }
}
// target 컨펌
function confirmTarget() {
  updateDisable()
  // validation check
  if ($('#target').val() == '') {
    return false;
  } else {
    return true;
  }
}
// popupStartDate 컨펌
function confirmPopupStartDate() {
  updateDisable()
  // validation check
  if ($('#popupStartDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// popupEndDate 컨펌
function confirmPopupEndDate() {
  updateDisable()
  // validation check
  if ($('#popupEndDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_left 컨펌
function confirmDisplayLeft() {
  updateDisable()
  // validation check
  if ($('#display_left').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_top 컨펌
function confirmDisplayTop() {
  updateDisable()
  // validation check
  if ($('#display_top').val() == '') {
    return false;
  } else {
    return true;
  }
}
// size 컨펌
function confirmSize() {
  updateDisable()
  // validation check
  if ($('#size').val() == '') {
    return false;
  } else {
    return true;
  }
}
// image_file 컨펌
function confirmImage() {
  updateDisable()
  // validation check
  if ($('#image_file').val() == '') {
    return false;
  } else {
    return true;
  }
}
// exposure 컨펌
function confirmExposure() {
  updateDisable()
  // validation check
  if ($('#exposure').val() == '') {
    return false;
  } else {
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmType()) {
    return;
  }
  if (!confirmTitle()) {
    return;
  }
  if (!confirmTarget()) {
    return;
  }
  if (!confirmPopupStartDate()) {
    return;
  }
  if (!confirmPopupEndDate()) {
    return;
  }
  if (!confirmDisplayLeft()) {
    return;
  }
  if (!confirmDisplayTop()) {
    return;
  }
  if (!confirmSize()) {
    return;
  }
  if (!confirmImage()) {
    return;
  }
  if (!confirmExposure()) {
    return;
  }
  $('.add_btn').addClass('on');
  $('.add_btn').attr('disabled', false);
}


// 취소 버튼
function gotoList() {
  document.location.href = 'popupList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo
}

// cancel 버튼 modal
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}


// Access 추가
function addPopup() {

  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText addPopup= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var popupId = data.results.id;
          linkPage();
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Unable to add Popup.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/admin/api/popup/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'type': $('#type').val(),
    'title': $('#title').val(),
    'target': $('#target').val(),
    'link': $('#link').val(),
    'popupStartDate': $('#popupStartDate').val(),
    'popupEndDate': $('#popupEndDate').val(),
    'displayLeft': $('#display_left').val(),
    'displayTop': $('#display_top').val(),
    'size': $('#size').val(),
    'popupFile': dataURL,
    'popupContent': $('#popupContent').val(),
    'exposure': $('#exposure').val()
  }));
}


// 사용자 리스트의 처음 pagination으로 이동
function linkPage() {
  document.location.href = 'popupList.html?selectVal=' + queryString.selectVal + '&pageNo=' + 1;
}
