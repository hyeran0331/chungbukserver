// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var dataURL = '';

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  // confirmSession()
  getPopupDetail()

  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })

  // 유효성 검사
  $('#type').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#title').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#target').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#popupStartDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#popupEndDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#display_left').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#display_top').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#size').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#exposure').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

  // 프로필 사진 file 변수(data에서 프로필사진 변경시 필요)
  var files;
  //profile image change
  var readURL = function(input) {
    if (input.files && input.files[0]) {
      if (input.files[0].size < 10 * 1024 * 1000) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image_form').find('img').empty();
          $('.image_form').find('img').attr('src', e.target.result)
          $('.image_form').find('img').attr('alt', 'popup image')
          dataURL = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        alert("error")
      }
    }
  }
  $('#image_file').on('change', function(file) {
    readURL(this);
    files = file.target.files[0];
  });

})

// 버튼 disabled 부여
function updateDisable() {
  $('.edit_btn').removeClass('on');
  $('.edit_btn').attr('disabled', true);
}

// type 컨펌
function confirmType() {
  updateDisable()
  // validation check
  if ($('#type').val() == '') {
    return false;
  } else {
    return true;
  }
}
// title 컨펌
function confirmTitle() {
  updateDisable()
  // validation check
  if ($('#title').val() == '') {
    return false;
  } else {
    return true;
  }
}
// target 컨펌
function confirmTarget() {
  updateDisable()
  // validation check
  if ($('#target').val() == '') {
    return false;
  } else {
    return true;
  }
}
// popupStartDate 컨펌
function confirmPopupStartDate() {
  updateDisable()
  // validation check
  if ($('#popupStartDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// popupEndDate 컨펌
function confirmPopupEndDate() {
  updateDisable()
  // validation check
  if ($('#popupEndDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_left 컨펌
function confirmDisplayLeft() {
  updateDisable()
  // validation check
  if ($('#display_left').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_top 컨펌
function confirmDisplayTop() {
  updateDisable()
  // validation check
  if ($('#display_top').val() == '') {
    return false;
  } else {
    return true;
  }
}
// size 컨펌
function confirmSize() {
  updateDisable()
  // validation check
  if ($('#size').val() == '') {
    return false;
  } else {
    return true;
  }
}
// exposure 컨펌
function confirmExposure() {
  updateDisable()
  // validation check
  if ($('#exposure').val() == '') {
    return false;
  } else {
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmType()) {
    return;
  }
  if (!confirmTitle()) {
    return;
  }
  if (!confirmTarget()) {
    return;
  }
  if (!confirmPopupStartDate()) {
    return;
  }
  if (!confirmPopupEndDate()) {
    return;
  }
  if (!confirmDisplayLeft()) {
    return;
  }
  if (!confirmDisplayTop()) {
    return;
  }
  if (!confirmSize()) {
    return;
  }
  if (!confirmExposure()) {
    return;
  }
  $('.edit_btn').addClass('on');
  $('.edit_btn').attr('disabled', false);
}


// 취소 버튼
function gotoList() {
  document.location.href = 'popupList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo
}

// cancel 버튼 modal
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}

//세션 확인하기
function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var duty = data.results.duty;

          // if (duty == 'ADMIN'){
          //   $('.button_container').append(editBtn, okBtn);
          // }else{
          //   $('.button_container').remove('.edit_btn_container, .ok_btn_container');
          // }

          getPopupDetail()

          // // 전체 데이터 로딩 화면 없애기
          // $('.allLoadData').fadeOut('slow', function() {
          //   $(this).remove();
          // })
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}


// popup detail 불러오기
function getPopupDetail() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText detailPopup= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var popupId = data.results.id;
          var type = data.results.type;
          var title = data.results.title;
          var target = data.results.target;
          var link = data.results.link;
          var popupStartDate = data.results.popupStartDate;
          var popupEndDate = data.results.popupEndDate;
          var displayLeft = data.results.displayLeft;
          var displayTop = data.results.displayTop;
          var size = data.results.size;
          var popupFile = data.results.popupFile;
          var popupContent = data.results.popupContent;
          var exposure = data.results.exposure;

          $('#type').val(type);
          $('#title').attr('value', title);
          $('#target').val(target);
          $('#link').attr('value', link);
          $('#popupStartDate').attr('value', popupStartDate);
          $('#popupEndDate').attr('value', popupEndDate);
          $('#display_left').attr('value', displayLeft);
          $('#display_top').attr('value', displayTop);
          $('#size').attr('value', size);
          $('.image_form').find('img').attr('src', popupFile)
          $('.image_form').find('img').attr('alt', 'popup image')
          $('#popupContent').val(popupContent);
          $('#exposure').val(exposure);

          // ok click event
          $('.edit_btn').click(function() {
            modifyPopup(popupId, popupFile)
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Unable to add Popup.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/popup/' + queryString.popupId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// Access Detail 데이터값 수정
function modifyPopup(popupId, popupFile) {
  if(dataURL == ''){
    dataURL = popupFile;
  }
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText modifyPopup= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 수정 완료시 이동
          gotoList()

        } else {
          hotelFalseOfDataSuccess(data.reasonCode, 'Failed to edit user')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/popup/' + popupId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'type': $('#type').val(),
    'title': $('#title').val(),
    'target': $('#target').val(),
    'link': $('#link').val(),
    'popupStartDate': $('#popupStartDate').val(),
    'popupEndDate': $('#popupEndDate').val(),
    'displayLeft': $('#display_left').val(),
    'displayTop': $('#display_top').val(),
    'size': $('#size').val(),
    'popupFile': dataURL,
    'popupContent': $('#popupContent').val(),
    'exposure': $('#exposure').val()
  }));
}
