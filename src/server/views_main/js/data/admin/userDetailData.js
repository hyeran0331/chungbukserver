// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var adminUserDuty = "";

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  confirmAdminSession()
})

// 취소 버튼
function goToList() {
  document.location.href = 'userList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
}

// cancel 버튼 modal
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    goToList()
  });
}

//세션 확인하기
function confirmAdminSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmAdminSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var adminId = "ADMINUSER"

          getUserDetail(adminId, adminUserDuty);
        } else {
          confirmUserSession()
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//세션 확인하기
function confirmUserSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmUserSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var userId = data.results.id;
          var userDuty = data.results.duty;

          getUserDetail(userId, userDuty);
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// Access Detail 데이터 불러오기
function getUserDetail(adminUserId, adminUserDuty) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var getDetailUserId = data.results.id;

          if(adminUserId == "ADMINUSER"){
            $('.ok_btn').removeClass('blind');
          }else if(getDetailUserId == adminUserId ){
            $('.ok_btn').removeClass('blind');
          }else if(adminUserDuty == "ADMIN"){
            $('.ok_btn').removeClass('blind');
          }

          var userDetailInfo = data.results;
          var date = yyyymmddDot_hh_mm_ss(userDetailInfo.created_at);

          // 배열의 각 요소의 값을 확인해 값을 변환시킨다.
          $.each(userDetailInfo, function(objectIndex, item) {
            if (item == null || item == "") {
              userDetailInfo = {
                ...userDetailInfo,
                [objectIndex]: '-'
              }
            }
          })

          var userId = userDetailInfo.id;
          var email = userDetailInfo.email;
          var backupEmail = userDetailInfo.backupEmail;
          var userName = userDetailInfo.userName;
          var engFirstName = userDetailInfo.engFirstName;
          var engLastName = userDetailInfo.engLastName;
          var job = userDetailInfo.job;
          var area = userDetailInfo.area;
          var phoneNumber = userDetailInfo.phoneNumber;
          var generalNumber = userDetailInfo.generalNumber;
          var affiliation = userDetailInfo.affiliation;
          var major = userDetailInfo.major;
          var position = userDetailInfo.position;
          var comTelephone = userDetailInfo.comTelephone;
          var comFax = userDetailInfo.comFax;
          var postalCode = userDetailInfo.postalCode;
          var address = userDetailInfo.address;
          var detailAddress = userDetailInfo.detailAddress;
          var grade = userDetailInfo.grade;
          var duty = userDetailInfo.duty;
          var memo = userDetailInfo.memo;

          // Access Detail 데이터 보여주기
          $('.info_email').find('span').html(email);
          $('.info_backup_email').find('span').html(backupEmail);
          $('.info_user_name').find('span').html(userName);
          $('.info_eng_first_name').find('span').html(engFirstName);
          $('.info_eng_last_name').find('span').html(engLastName);
          $('.info_job').find('span').html(job);
          $('.info_area').find('span').html(area);
          $('.info_phone_number').find('span').html(phoneNumber);
          $('.info_general_number').find('span').html(generalNumber);
          $('.info_affiliation').find('span').html(affiliation);
          $('.info_major').find('span').html(major);
          $('.info_position').find('span').html(position);
          $('.info_com_telephone').find('span').html(comTelephone);
          $('.info_com_fax').find('span').html(comFax);
          $('.info_postal_code').find('span').html(postalCode);
          $('.info_address').find('span').html(address);
          $('.info_detail_address').find('span').html(detailAddress);
          $('.info_date').find('span').html(date);
          $('.info_grade').find('select').val(grade).prop('selected', true);
          $('.info_duty').find('select').val(duty).prop('selected', true);
          $('.info_memo').find('textarea').html(memo);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
        adminFalseOfDataSuccess(data.reasonCode, 'Failed to get user information')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/user/' + queryString.userId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 사용자의 권한 변경
function modifyUser() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText modifyUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          document.location.href = 'userList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'User info change failed');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/api/user/' + queryString.userId + '/changeUserInfo', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'grade': $('#userGradeSelect').val(),
    'duty': $('#userDutySelect').val(),
    'memo': $("#memo_view").val()
  }));
}
