// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  getUserDetail()
})

// 취소 버튼
function cancel() {
  document.location.href = 'adminUserList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
}

function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    cancel()
  });
}

// 사용자의 권한 변경
function modifyUser() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText modifyUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          document.location.href = 'adminUserList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Duty change failed');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/user/' + queryString.userId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'duty': $('#dutyUser').val()
  }));
}

//user detail data 불러오기
function getUserDetail() {
    // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var email = data.results.email;
          var name = data.results.name;
          var department = data.results.department;
          var duty = data.results.duty;
          var date = yyyymmddDot_hh_mm_ss(data.results.loginAt);

          $('.info_email').find('span').text(email);
          $('.info_name').find('span').text(name);
          $('.info_com').find('span').text(department);
          $('.info_duty').find('span').text(duty);
          $('.info_date').find('span').text(date);

          if (duty == 'ADMIN') {
            $('#dutyAdmin').addClass('duty_admin');
            $('.cancel_btn').click(function() {
              cancel();
            });
          } else if (duty == 'USER') {
            $('#dutyUser').addClass('duty_user');
            $('#ok_btn').addClass('ok_btn');
            $('.cancel_btn').click(function() {
              cancelMessagePopup();
            });
          }

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/user/' + queryString.userId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
