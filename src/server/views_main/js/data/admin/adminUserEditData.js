// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var nameRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var departmentRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var spaceRegExp = RegExp(/^[\s]+$/);

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  getUserSession()

  $('#name').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#department').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

})

// edit user info 보여주기
function myEmailSession(myEmail, myName, myDepartment) {
  $('#email').attr('placeholder', myEmail)
  $('#name').attr('value', myName)
  $('#department').attr('value', myDepartment)
}

// 탈퇴하기
function withdrawal(target) {
  deleteAdmin(target.attr('data-value'))
}

// 버튼 disabled 부여
function updateDisable() {
  $('.save_btn').removeClass('on');
  $('.save_btn').attr('disabled', true);
}

// 이름 컨펌
function confirmName() {
  updateDisable()
  // validation check
  if ($('#name').val() == '' || spaceRegExp.test($('#name').val()) == true) {
    $('.message').find('span').text('* 이름을 입력해주세요.');
  } else if (!nameRegExp.test($('#name').val())) {
    $('.message').find('span').text("* 한글과 영문 또는 '. _ -'를 사용해서 입력해주세요.");
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}
// 소속부서 컨펌
function confirmDepartment() {
  updateDisable()
  // validation check
  if ($('#department').val() == '' || spaceRegExp.test($('#department').val()) == true) {
    $('.message').find('span').text('* 소속을 입력해주세요.');
  } else if (!departmentRegExp.test($('#department').val())) {
    $('.message').find('span').text("* 한글과 영문 또는 '. _ -'를 사용해서 입력해주세요.");
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmName()) {
    return;
  }
  if (!confirmDepartment()) {
    return;
  }
  $('.save_btn').addClass('on');
  $('.save_btn').attr('disabled', false);
}

//세션 확인하기
function getUserSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var myEmail = data.results.email;
          var myAdminId = data.results.id;
          var myName = data.results.name;
          var myDepartment = data.results.department;

          myEmailSession(myEmail, myName, myDepartment);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })
          // modify 하기
          $('.save_btn').click(function() {
            modifyUser(myAdminId)
          })
          // delete 하기
          $('.withdrawalBtn').attr('data-value', myAdminId);

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 사용자를 추가
function modifyUser(myAdminId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText modifyUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var editName = $('#name').val();
          var editDepartment = $('#department').val();

          // menu userInfo에 modify된 유저의 정보를 수정한다.
          $('#userName', window.parent.document).empty();
          $('#userName', window.parent.document).html(editName);
          $('#userCompany', window.parent.document).empty();
          $('#userCompany', window.parent.document).html(editDepartment)

          document.location.href = 'adminUserList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to edit user')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/user/' + myAdminId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'name': $('#name').val(),
    'department': $('#department').val()
  }));
}

//관리자 탈퇴
function deleteAdmin(myAdminId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseTsext deleteAdmin= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {

          parent.location.href = '../index.html'

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get withdraw.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/admin/api/user/' + myAdminId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
