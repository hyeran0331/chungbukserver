// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  getTransferDetail()
})

// 취소 버튼
function goList() {
  // document.location.href = 'transferLogList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo + '&fromDate=' + queryString.fromDate + '&endDate=' + queryString.endDate;
  document.location.href = 'transferLogList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo;

}

//transfer detail data 불러오기
function getTransferDetail() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText gettransferDetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var transferId = data.results.id;
          var type = data.results.type;
          var loginType = data.results.loginType;
          var user = data.results.user;
          var path = data.results.path;
          var createdAt = yyyymmddDot_hh_mm_ss(data.results.created_at);
          var updatedAt = yyyymmddDot_hh_mm_ss(data.results.updated_at);

          $('.info_type').find('span').html(type);
          $('.info_login_type').find('span').html(loginType);
          $('.info_path').find('span').html(path);
          $('.info_created').find('span').html(createdAt);
          $('.info_updated').find('span').html(updatedAt);

          var statusHeader = data.results.header;
          if (type == "RESPONSE") {
            // type이 response 일때 JSON 정렬
            var statusRow = '<div class="detail_cell index">Status</div><div class="detail_cell content info_status"><span></span></div>';
            $('.transfer_info').find('.statusHeader').append(statusRow);
            $('.info_status').find('span').html(statusHeader);
          } else if (type == "REQUEST") {
            // type이 request 일때 JSON 정렬
            var headerRow = '<div class="detail_cell index">Header</div><div class="detail_cell content info_header"><span></span></div>';
            $('.transfer_info').find('.statusHeader').append(headerRow);
            var jsonHeader = JSON.parse(statusHeader);
            $('.info_header').find('span').empty();
            $('.info_header').find('span').append(JSON.stringify(jsonHeader, null, 4))
          }

          var param = data.results.param;
          // param JSON 정렬
          var jsonParam = JSON.parse(param);
          $('.info_param').find('span').empty();
          $('.info_param').find('span').append(JSON.stringify(jsonParam, null, 4))

          var body = data.results.body;
          // body JSON 정렬
          var jsonBody = JSON.parse(body);
          $('.info_dody').find('span').empty();
          $('.info_dody').find('span').append(JSON.stringify(jsonBody, null, 4))

          if (data.results.user == null || data.results.user == "") {
            $('.info_user').find('span').html(" - ");
          } else {
            $('.info_user').find('span').html(user);
          }

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/transferLog/' + queryString.transferId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
