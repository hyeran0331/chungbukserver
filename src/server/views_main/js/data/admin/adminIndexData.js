var myId;

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  // getAdminSession();
  getUserSession();
})

//menu Link 이동
function menuLink(target) {
  event.preventDefault();
  var thisLink = target.attr('href');
  var iframe = $('iframe');
  var iframeSrc = iframe.attr('src');
  var srcChange = iframeSrc.replace(iframeSrc, thisLink);

  target.parent().siblings().removeClass('on');
  target.parent().addClass('on');
  iframe.attr('src', srcChange);
}

// 세션 불러오기
function getAdminSession() {
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getAdminSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var email = data.session;
          var name = data.results.name;
          var department = data.results.department;
          var duty = data.results.duty;

          setUserToken(email)
          addMenuInfo(name, department, email)

          $('.menu_inner').find('.adminUser').prepend(adminUserLi);

          // if (duty == "ADMIN") {
          //   $('.menu_inner').find('.adminUser').prepend(adminUserLi);
          // }
        } else {
          console.log("not admin session");
          getUserSession();
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 세션 불러오기
function getUserSession() {
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getUserSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var email = data.session;
          var userName = data.results.userName;
          var affiliation = data.results.affiliation;
          var duty = data.results.duty;

          setUserToken(email)
          addMenuInfo(userName, affiliation, email)

          if (duty != "ADMIN") {
            alert("you can not enter this path")
            document.location.href = '../index.html';
          }

        } else {
          console.log("not user session");
          document.location.href = '../index.html';
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 로그아웃하기
function logoutAdmin() {
  event.preventDefault();
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText = " + httpRequest.responseText);
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          window.location.href = '../index.html'
        } else {
          console.log("admin logout fail");
          logoutUser()
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('delete', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 로그아웃하기
function logoutUser() {
  event.preventDefault();
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText = " + httpRequest.responseText);
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          window.location.href = '../index.html'

        } else {
          console.log("user logout fail");
          window.location.reload()
        }
      } else {
        console.log('code:' + httpRequest.status);
        window.location.reload()
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
