// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
var dataURL = '';

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  // confirmSession()
  getBannerDetail()

  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })

  // 유효성 검사
  $('#position').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#title').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#target').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#bannerStartDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#bannerEndDate').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#exposure').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#turn').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

  // 프로필 사진 file 변수(data에서 프로필사진 변경시 필요)
  var files;
  //profile image change
  var readURL = function(input) {
    if (input.files && input.files[0]) {
      if (input.files[0].size < 10 * 1024 * 1000) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.image_form').find('img').empty();
          $('.image_form').find('img').attr('src', e.target.result)
          $('.image_form').find('img').attr('alt', 'banner image')
          dataURL = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
      } else {
        alert("error")
      }
    }
  }
  $('#image_file').on('change', function(file) {
    readURL(this);
    files = file.target.files[0];
  });

})

// 버튼 disabled 부여
function updateDisable() {
  $('.add_btn').removeClass('on');
  $('.add_btn').attr('disabled', true);
}

// type 컨펌
function confirmPosition() {
  updateDisable()
  // validation check
  if ($('#position').val() == '') {
    return false;
  } else {
    return true;
  }
}
// title 컨펌
function confirmTitle() {
  updateDisable()
  // validation check
  if ($('#title').val() == '') {
    return false;
  } else {
    return true;
  }
}
// target 컨펌
function confirmTarget() {
  updateDisable()
  // validation check
  if ($('#target').val() == '') {
    return false;
  } else {
    return true;
  }
}
// bannerStartDate 컨펌
function confirmBannerStartDate() {
  updateDisable()
  // validation check
  if ($('#bannerStartDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// bannerEndDate 컨펌
function confirmBannerEndDate() {
  updateDisable()
  // validation check
  if ($('#bannerEndDate').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_left 컨펌
function confirmExposure() {
  updateDisable()
  // validation check
  if ($('#exposure').val() == '') {
    return false;
  } else {
    return true;
  }
}
// display_top 컨펌
function confirmTurn() {
  updateDisable()
  // validation check
  if ($('#turn').val() == '') {
    return false;
  } else {
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmPosition()) {
    return;
  }
  if (!confirmTitle()) {
    return;
  }
  if (!confirmTarget()) {
    return;
  }
  if (!confirmBannerStartDate()) {
    return;
  }
  if (!confirmBannerEndDate()) {
    return;
  }
  if (!confirmExposure()) {
    return;
  }
  if (!confirmTurn()) {
    return;
  }
  $('.add_btn').addClass('on');
  $('.add_btn').attr('disabled', false);
}


// 취소 버튼
function gotoList() {
  document.location.href = 'bannerList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo
}

// cancel 버튼 modal
function cancelMessagebanner() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}

//세션 확인하기
function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var duty = data.results.duty;

          // if (duty == 'ADMIN'){
          //   $('.button_container').append(editBtn, okBtn);
          // }else{
          //   $('.button_container').remove('.edit_btn_container, .ok_btn_container');
          // }

          getbannerDetail()

          // // 전체 데이터 로딩 화면 없애기
          // $('.allLoadData').fadeOut('slow', function() {
          //   $(this).remove();
          // })
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}


// banner detail 불러오기
function getBannerDetail() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getBannerDetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var bannerId = data.results.id;
          var position = data.results.position;
          var title = data.results.title;
          var bannerContent = data.results.bannerContent;
          var target = data.results.target;
          var link = data.results.link;
          var bannerStartDate = data.results.bannerStartDate;
          var bannerEndDate = data.results.bannerEndDate;
          var exposure = data.results.exposure;
          var turn = data.results.turn;
          var bannerFile = data.results.bannerFile;

          $('#position').val(position);
          $('#title').attr('value', title);
          $('#banner_content').attr('value', bannerContent);
          $('#target').val(target);
          $('#link').attr('value', link);
          $('#bannerStartDate').attr('value', bannerStartDate);
          $('#bannerEndDate').attr('value', bannerEndDate);
          $('#exposure').val(exposure);
          $('#turn').attr('value', turn);
          $('.image_form').find('img').attr('src', bannerFile)
          $('.image_form').find('img').attr('alt', 'banner image')

          // ok click event
          $('.edit_btn').click(function() {
            modifyBanner(bannerId, bannerFile)
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Unable to add banner.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/banner/' + queryString.bannerId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// Access Detail 데이터값 수정
function modifyBanner(bannerId, bannerFile) {
  if(dataURL == ''){
    dataURL = bannerFile;
  }
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText modifybanner= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 수정 완료시 이동
          gotoList()

        } else {
          hotelFalseOfDataSuccess(data.reasonCode, 'Failed to edit user')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/banner/' + bannerId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'position': $('#position').val(),
    'title': $('#title').val(),
    'bannerContent': $('#banner_content').val(),
    'target': $('#target').val(),
    'link': $('#link').val(),
    'bannerStartDate': $('#bannerStartDate').val(),
    'bannerEndDate': $('#bannerEndDate').val(),
    'exposure': $('#exposure').val(),
    'turn': $('#turn').val(),
    'bannerFile': dataURL,
  }));
}
