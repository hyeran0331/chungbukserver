// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {

  // select 값이 변하면 변한 값에 맞게 input 속성을 수정한다.
  $("#userlist_select").on('change', function() {
    var selectVal = $(this).val();

    function selectOn() {
      $('#search_input').attr('readonly', false)
      $('#search_input').attr('placeholder', '검색어를 입력해주세요.')
    }
    if (selectVal == 'name') {
      selectOn();
    } else if (selectVal == 'email') {
      selectOn()
    } else if (selectVal == 'department') {
      selectOn()
    } else if (selectVal == 'duty') {
      selectOn()
    } else if (selectVal == '0') {
      $('#search_input').val('');
      $('#search_input').attr('readonly', true)
      $('#search_input').attr('placeholder', '')
      $('#userlist_select').val('0');
    }
  })

  // delete click시 체크유무 판단후 popup띄우기
  $('#deleteBtn').click(function() {
    if ($('input[name="eachCheck"]').is(':checked') == true) {
      deleteMessagePopup()
    } else if ($('input[name="eachCheck"]').is(':checked') == false) {
      cantDeleteList(noDeleteMessage)
    }
  })

  if (JSON.stringify(queryString) === JSON.stringify({})) {
    getPageList(1)
  } else {
    var select = $('#userlist_select');
    var input = $('#search_input');
    select.val(queryString.selectVal);
    input.val(queryString.inputVal);

    // reload 시 select val 에 따라 input 의 속성을 수정한다.
    if (select.val() == '0') {
      $('#search_input').attr('readonly', true)
    } else {
      $('#search_input').attr('readonly', false)
    }

    // empty list 에서 reload시 pageNo 값을 정의 한다.
    if (queryString.pageNo == '') {
      getSearchList(1)
    } else {
      getSearchList(queryString.pageNo)
    }
  }

});


// 추가 버튼
function addBtn() {
  var selectVal = $('#userlist_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $('.pagination').find('strong').text();

  document.location.href = 'adminUserAdd.html?selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo
}

//모든 체크박스를 선택한다.
function allCheck() {
  function checkboxOn() {
    $('.checkboxImg').attr('src', 'images/checkbox_on.svg')
  }

  function checkboxOff() {
    $('.checkboxImg').attr('src', 'images/checkbox_off.svg')
  }
  if ($('#allCheck').is(':checked') == true) {
    $('input[name="eachCheck"]').prop('checked', true);
    checkboxOn()
  } else {
    $('input[name="eachCheck"]').prop('checked', false);
    checkboxOff()
  }
}

// 해당 목록을 삭제한다.
var removeView = [];
var removeList = [];
var adminList = [];

// 다중리스트 삭제
function deleteList() {
  $('input[name="eachCheck"]:checked').each(function() {
    removeList.push($(this).attr('id'));
  });
  confirmAdmin()
}

// 삭제목록에 admin이 존재하는지 확인
function confirmAdmin() {
  for (var i = 0; i < removeList.length; i++) {
    if ($('input#' + removeList[i]).parent().parent().children().eq(4).text() == 'ADMIN') {
      adminList.push(removeList[i]);
    }
  }
  if (adminList.length > 0) {
    removeList = [];
    adminList = [];
    cantDeleteList(adminCantDeleteMessage)
  } else {
    if (removeList.length > 0) {
      deleteOne(removeList[0]);
    }
  }
}

// 리스트 하나 삭제
function deleteOne(userId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText deleteOne= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          removeList.splice(0, 1);
          if (removeList.length > 0) {
            deleteOne(removeList[0]);
          } else {
            for (var i = 0; i < removeView.length; i++) {
              $('input#' + removeView[0]).parent().parent().remove();
              removeView.splice(0, 1);
            }
            // 체크박스 해제
            if ($('#allCheck').is(':checked') == true) {
              $('#allCheck').prop('checked', false);
            }
            // 새로 목록 가져오기
            var pageNum = $(document).find('strong').text();
            getSearchList(pageNum);
            $('.delete_btn').blur();
          }
        } else {
          for (var i = 0; i < removeView.length; i++) {
            $('input#' + removeView[0]).parent().parent().remove();
            removeView.splice(0, 1);
          }
          // 새로 목록 가져오기
          var pageNum = $(document).find('strong').text();
          getSearchList(pageNum);
          $('.delete_btn').blur();
          // 체크박스 해제
          if ($('#allCheck').is(':checked') == true) {
            $('#allCheck').prop('checked', false);
          }
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to Delete');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/admin/api/user/' + userId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 체크박스 클릭
function labelClick(event, target) {
  event.stopImmediatePropagation();
}

function inputClick(event, target) {
  event.stopImmediatePropagation();
  target.each(function() {
    if ($(this).is(':checked') == false) {
      $('#allCheck').prop('checked', false);
    }
  });
}

// 사용자 정보 이동
function linkUserDetail(id) {
  var selectVal = $('#userlist_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $('.pagination').find('strong').text();
  // if (!selectVal) {
  //   selectVal = ''
  // }
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText = " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var detailEmail = data.session;
          var detailId = data.results.id;
          // 본인일 경우 adminuserEdit.html
          if (id == detailId) {
            document.location.href = 'adminUserEdit.html?userId=' + id + '&selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo;
          }
          // 다른유저일 경우 adminuserDetail.html
          else {
            document.location.href = 'adminUserDetail.html?userId=' + id + '&selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo;
          }
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.');
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// 리스트 목록 가져오기
function getPageList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = Paging(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var id = data.results.rows[i].id;
              var name = data.results.rows[i].name;
              var email = data.results.rows[i].email;
              var department = data.results.rows[i].department;
              var duty = data.results.rows[i].duty;
              var date = yyyymmddDot_hh_mm_ss(data.results.rows[i].loginAt);

              if (data.results.rows[i].loginAt == null) {
                addUserList(id, name, email, department, duty, '-', index)
              } else {
                addUserList(id, name, email, department, duty, date, index)
              }
            }
          } else {
            $('.tbody').empty();
            $('.tbody').append(empty);
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get the corresponding page list.')
        }

      } else {
        console.log('code:' + httpRequest.status);
      }

    }
  }
  httpRequest.open('get', serverURL + '/admin/api/user/?email&name&department&duty&offset=' + 10 * (pageNo - 1) + '&limit=' + 10, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
Paging = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getPageList(' + s2 + ')>');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');
    } else {
      html.push('<a href=javascript:getPageList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getPageList(' + nextPageNum + ')>');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}

// 검색 엔터키
function enterkey() {
  if (window.event.keyCode == 13) {
    search();
  }
}

// 검색하기
function search() {
  var selectVal = $('#userlist_select').val();
  var inputVal = $('#search_input').val();
  getSearchList(1);
}

// search url
function makeSearchUrl(offsetNum) {
  var selectVal = $('#userlist_select').val();
  var inputVal = $('#search_input').val();
  var name = ""
  var email = ""
  var department = ""
  var duty = ""

  if (selectVal == 'name') {
    name = inputVal
  } else if (selectVal == 'email') {
    email = inputVal
  } else if (selectVal == 'department') {
    department = inputVal
  } else if (selectVal == 'duty') {
    duty = inputVal
  }

  var getListUrl = '/admin/api/user/?email=' + email + '&name=' + name + '&department=' + department + '&duty=' + duty + '&offset=' + offsetNum + '&limit=' + 10;
  return getListUrl
}

// 검색된 리스트
function getSearchList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getSearchList= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = PagingSearch(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var id = data.results.rows[i].id;
              var name = data.results.rows[i].name;
              var email = data.results.rows[i].email;
              var department = data.results.rows[i].department;
              var duty = data.results.rows[i].duty;
              var date = yyyymmddDot_hh_mm_ss(data.results.rows[i].loginAt);

              if (data.results.rows[i].loginAt == null) {
                addUserList(id, name, email, department, duty, '-', index)
              } else {
                addUserList(id, name, email, department, duty, date, index)
              }
            }

          } else {
            if (pageNo == '1') {
              $('.tbody').empty();
              $('.tbody').append(empty);
            } else {
              getSearchList(pageNo - 1)
            }
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          //전체 페이지 로딩 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get list of search pages.');
        }

      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  let searchUrl = makeSearchUrl((pageNo - 1) * 10)
  httpRequest.open('get', serverURL + searchUrl, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
PagingSearch = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getSearchList(' + s2 + ')>');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');

    } else {
      html.push('<a href=javascript:getSearchList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getSearchList(' + nextPageNum + ')>');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}
