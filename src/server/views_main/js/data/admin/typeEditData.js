// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  console.log("typeEdit = "+queryString);


  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut('slow', function() {
    $(this).remove();
  })

  // 유효성 검사
  $('#title').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#description').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })

})

// 버튼 disabled 부여
function updateDisable() {
  $('.add_btn').removeClass('on');
  $('.add_btn').attr('disabled', true);
}

// 컨펌
function confirmTitle() {
  updateDisable()
  // validation check
  if ($('#title').val() == '') {
    return false;
  } else {
    return true;
  }
}
// 컨펌
function confirmDescription() {
  updateDisable()
  // validation check
  if ($('#description').val() == '') {
    return false;
  } else {
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmTitle()) {
    return;
  }
  if (!confirmDescription()) {
    return;
  }
  $('.add_btn').addClass('on');
  $('.add_btn').attr('disabled', false);
}


// 취소 버튼
function gotoList() {
  document.location.href = 'typeDetail.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo
}

// 사용자 리스트의 처음 pagination으로 이동
function linkPage() {
  document.location.href = 'typeDetail.html?selectVal=' + queryString.selectVal + '&pageNo=' + 1;
}

// cancel 버튼 modal
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}

// Type 수정
function getType() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        console.log("httpRequest.responseText getType= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 추가 완료시 이동
          // linkPage()
          alert("get success")
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Unable to add Type.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/type/' + queryString.typeId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

// Type 수정
function EditType() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText EditType= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 추가 완료시 이동
          // linkPage()
          alert("edit success")
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Unable to add Type.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/type/' + queryString.typeId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'title': $('#title').val(),
    'description': $('#description').val()
  }));
}
