// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {
  confirmSession()
})

// 취소 버튼
function gotoList() {
  document.location.href = 'qnaList.html?selectVal=' + queryString.selectVal + '&inputVal=' + queryString.inputVal + '&pageNo=' + queryString.pageNo
}

// cancel 버튼 modal
function cancelMessagePopup() {
  modalMainBody.prepend(errorMessageHtml);
  modalMainBody.find('.errorMessage').find('.errorText').html(cancelMessage);
  modalMainBody.find('.errorMessage').find('.cancel').click(function() {
    modalMainBody.find('#modalMainMask').fadeOut(function() {
      $(this).remove();
    });
    modalMainBody.find('.errorMessage').fadeOut(function() {
      $(this).remove();
    });
  });
  modalMainBody.find('.errorMessage').find('.ok').click(function() {
    modalMainBody.find('#modalMainMask').remove();
    modalMainBody.find('.errorMessage').remove();

    gotoList()
  });
}

//세션 확인하기
function confirmSession() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText confirmSession= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var myId = data.results.id;
          getQnADetail();

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get session.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/api/auth/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//  데이터 불러오기
function getQnADetail() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getQnADetail= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var qnaId = data.results.id;
          var title = data.results.title;
          var state = data.results.state;
          var questionDate = yyyymmddDot_hh_mm_ss(data.results.questionDate);

          $('.info_title').find('span').html(title);
          $('.info_date').find('span').html(questionDate);
          $('#stateSelect').val(state);

          var questionContent = data.results.question;
          var userName = data.results.user.userName;
          var userEmail = data.results.user.email;
          var userNumber = data.results.user.phoneNumber;

          $('.question_content').find('span').html(questionContent);
          $('.user_info_container').find('.user_name').html(userName);
          $('.user_info_container').find('.user_email').html(userEmail);
          $('.user_info_container').find('.user_number').html(userNumber);

          var answerContent = data.results.answer;
          $('#answer').val(answerContent);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

          // ok click event
          $('.edit_btn').click(function() {
            modifyQnA(qnaId)
          })

        } else {
          var failReason = data.reason;
          adminFalseOfDataSuccess(data.reasonCode, failReason)
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/qna/' + queryString.qnaId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

function modifyQnA(qnaId){
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText modifyQnA= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          // 수정 완료시 이동
          gotoList()

        } else {
          var failReason = data.reason;
          adminFalseOfDataSuccess(data.reasonCode, failReason)
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/admin/api/qna/answer/' + qnaId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'state': $('#stateSelect').val(),
    'answer': $('#answer').val()
  }));
}
