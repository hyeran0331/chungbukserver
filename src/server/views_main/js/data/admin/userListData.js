// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {

  // delete click시 체크유무 판단후 popup띄우기
  $('#deleteBtn').click(function() {
    if ($('input[name="eachCheck"]').is(':checked') == true) {
      deleteMessagePopup()
    } else if ($('input[name="eachCheck"]').is(':checked') == false) {
      cantDeleteList(noDeleteMessage)
    }
  })

  // delete click시 체크유무 판단후 popup띄우기
  $('#changeGradeBtn').click(function() {
    if ($('input[name="eachCheck"]').is(':checked') == true) {
      changeGradeMessagePopup()
    } else if ($('input[name="eachCheck"]').is(':checked') == false) {
      cantDeleteList(noChangeGradeMessage)
    }
  })

  if (JSON.stringify(queryString) === JSON.stringify({})) {
    getPageList(1)
  } else {
    var select = $('#user_select');
    var input = $('#search_input');
    select.val(queryString.selectVal);
    input.val(queryString.inputVal);

    // reload 시 select val 에 따라 input 의 속성을 수정한다.
    if (select.val() == '0') {
      $('#search_input').attr('readonly', true)
    } else {
      $('#search_input').attr('readonly', false)
    }

    // empty list 에서 reload시 pageNo 값을 정의 한다.
    if (queryString.pageNo == '') {
      getSearchList(1)
    } else {
      getSearchList(queryString.pageNo)
    }
  }

});

//모든 체크박스를 선택한다.
function allCheck() {
  function checkboxOn() {
    $('.checkboxImg').attr('src', 'images/checkbox_on.svg')
  }

  function checkboxOff() {
    $('.checkboxImg').attr('src', 'images/checkbox_off.svg')
  }
  if ($('#allCheck').is(':checked') == true) {
    $('input[name="eachCheck"]').prop('checked', true);
    checkboxOn()
  } else {
    $('input[name="eachCheck"]').prop('checked', false);
    checkboxOff()
  }
}

// 해당 유저의 grade 를 변경한다.
var chageGradeView = [];
var chageGradeList = [];

// 다중리스트 changeGrade
function changeGradeList() {
  $('input[name="eachCheck"]:checked').each(function() {
    chageGradeList.push($(this).attr('id'));
  });
  if (chageGradeList.length > 0) {
    changeGradeOne(chageGradeList[0]);
  }
}
// 리스트 하나 changeGrade
function changeGradeOne(userId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText changeGradeOne= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          chageGradeView.push(changeGradeList[0]);
          chageGradeList.splice(0, 1);

          if (chageGradeList.length > 0) {
            changeGradeOne(chageGradeList[0]);
          } else {
            for (var i = 0; i < chageGradeView.length; i++) {
              $('input#' + chageGradeView[0]).parent().parent().remove();
              chageGradeView.splice(0, 1);
            }
            // 체크박스 해제
            if ($('#allCheck').is(':checked') == true) {
              $('#allCheck').prop('checked', false);
            }
            // 새로 목록 가져오기
            var pageNum = $(document).find('strong').text();
            getSearchList(pageNum);
            $('.change_grade_btn').blur();
          }
        } else {
          for (var i = 0; i < chageGradeView.length; i++) {
            $('input#' + chageGradeView[0]).parent().parent().remove();
            chageGradeView.splice(0, 1);
          }
          // 새로 목록 가져오기
          var pageNum = $(document).find('strong').text();
          getSearchList(pageNum);
          $('.change_grade_btn').blur();
          // 체크박스 해제
          if ($('#allCheck').is(':checked') == true) {
            $('#allCheck').prop('checked', false);
          }
          adminFalseOfDataSuccess(data.reasonCode, 'change grade failed.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('put', serverURL + '/api/user/' + userId + '/changeUserInfo', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'grade': $('#change_grade_select').val(),
  }));
}


// 해당 목록을 삭제한다.
var removeView = [];
var removeList = [];

// 다중리스트 delete
function deleteList() {
  $('input[name="eachCheck"]:checked').each(function() {
    removeList.push($(this).attr('id'));
  });
  if (removeList.length > 0) {
    deleteOne(removeList[0]);
  }
}

// 리스트 하나 delete
function deleteOne(userId) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText deleteOne= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          removeView.push(deleteList[0]);
          removeList.splice(0, 1);

          if (removeList.length > 0) {
            deleteOne(removeList[0]);
          } else {
            for (var i = 0; i < removeView.length; i++) {
              $('input#' + removeView[0]).parent().parent().remove();
              removeView.splice(0, 1);
            }
            // 체크박스 해제
            if ($('#allCheck').is(':checked') == true) {
              $('#allCheck').prop('checked', false);
            }
            // 새로 목록 가져오기
            var pageNum = $(document).find('strong').text();
            getSearchList(pageNum);
            $('.delete_btn').blur();
          }
        } else {
          for (var i = 0; i < removeView.length; i++) {
            $('input#' + removeView[0]).parent().parent().remove();
            removeView.splice(0, 1);
          }
          // 새로 목록 가져오기
          var pageNum = $(document).find('strong').text();
          getSearchList(pageNum);
          $('.delete_btn').blur();
          // 체크박스 해제
          if ($('#allCheck').is(':checked') == true) {
            $('#allCheck').prop('checked', false);
          }
          adminFalseOfDataSuccess(data.reasonCode, 'delete failed.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('delete', serverURL + '/api/user/' + userId, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
// 체크박스 클릭
function labelClick(event, target) {
  event.stopImmediatePropagation();
}

function inputClick(event, target) {
  event.stopImmediatePropagation();
  target.each(function() {
    if ($(this).is(':checked') == false) {
      $('#allCheck').prop('checked', false);
    }
  });
}

// version 정보 이동
function linkUserDetail(id) {
  var selectVal = $('#user_select').val();
  var inputVal = $('#search_input').val();
  var pageNo = $('.pagination').find('strong').text();
  // version 정보로 이동
  document.location.href = 'userDetail.html?userId=' + id + '&selectVal=' + selectVal + '&inputVal=' + inputVal + '&pageNo=' + pageNo;
}

// 리스트 목록 가져오기
function getPageList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        // console.log("httpRequest.responseText getSearchList= " + httpRequest.responseText)
        //서버 응답 결과에 따라 알맞은 작업 처리
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = Paging(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var id = data.results.rows[i].id;
              var userName = data.results.rows[i].userName;
              var email = data.results.rows[i].email;
              var phoneNumber = data.results.rows[i].phoneNumber;
              var affiliation = data.results.rows[i].affiliation;
              var grade = data.results.rows[i].grade;
              var date = yyyymmddDot_hh_mm_ss(data.results.rows[i].created_at);

              if (data.results.rows[i].loginAt == null) {
                addUserList(id, userName, email, affiliation, grade, phoneNumber, '-', index)
              } else {
                addUserList(id, userName, email, affiliation, grade, phoneNumber, date, index)
              }
            }
          } else {
            $('.tbody').empty();
            $('.tbody').append(empty);
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          // 전체 데이터 로딩 화면 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get the corresponding page list.')
        }

      } else {
        console.log('code:' + httpRequest.status);
      }

    }
  }
  httpRequest.open('get', serverURL + '/api/user/?content&userName&phoneNumber&email&affiliation&grade&offset=' + 10 * (pageNo - 1) + '&limit=' + 10, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
Paging = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getPageList(' + s2 + ')>');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');
    } else {
      html.push('<a href=javascript:getPageList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getPageList(' + nextPageNum + ')>');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}

// 검색 엔터키
function enterkey() {
  if (window.event.keyCode == 13) {
    search();
  }
}

// 검색하기
function search() {
  var selectVal = $('#user_select').val();
  var inputVal = $('#search_input').val();
  getSearchList(1);
}

// search url
function makeSearchUrl(offsetNum) {
  var selectVal = $('#user_select').val();
  var inputVal = $('#search_input').val();
  var content = ""
  var userName = ""
  var phoneNumber = ""
  var email = ""
  var affiliation = ""
  var grade = ""


  if (selectVal == 'content') {
    content = inputVal
  } else if (selectVal == 'userName') {
    userName = inputVal
  } else if (selectVal == 'phoneNumber') {
    phoneNumber = inputVal
  } else if (selectVal == 'email') {
    email = inputVal
  } else if (selectVal == 'affiliation') {
    affiliation = inputVal
  } else if (selectVal == 'grade') {
    grade = inputVal
  }

  var getListUrl = '/api/user/?content=' + content + '&userName=' + userName + '&phoneNumber=' + phoneNumber + '&email=' + email + '&affiliation=' + affiliation + '&grade=' + grade + '&offset=' + offsetNum + '&limit=' + 10;
  return getListUrl
}

// 검색된 리스트
function getSearchList(pageNo) {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText getSearchList= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var item = data.results.rows;
          var len = item.length;
          var page = data.results.count;

          //페이징 변수
          var page_boardList = PagingSearch(page, 10, 10, pageNo); //공통 페이징 처리 함수 호출

          //데이타 그리기
          if (len > 0) {
            $('.tbody').empty();

            // 사용자 목록을 생성한다.
            for (var i = 0; i < data.results.rows.length; i++) {
              var index = (i + 1) + (pageNo - 1) * 10;
              var id = data.results.rows[i].id;
              var userName = data.results.rows[i].userName;
              var email = data.results.rows[i].email;
              var phoneNumber = data.results.rows[i].phoneNumber;
              var affiliation = data.results.rows[i].affiliation;
              var grade = data.results.rows[i].grade;
              var date = yyyymmddDot_hh_mm_ss(data.results.rows[i].created_at);

              if (data.results.rows[i].loginAt == null) {
                addUserList(id, userName, email, affiliation, grade, phoneNumber, '-', index)
              } else {
                addUserList(id, userName, email, affiliation, grade, phoneNumber, date, index)
              }
            }

          } else {
            if (pageNo == '1') {
              $('.tbody').empty();
              $('.tbody').append(empty);
            } else {
              getSearchList(pageNo - 1)
            }
          }

          //페이징 그리기
          $('.pagination').empty().html(page_boardList);

          //전체 페이지 로딩 없애기
          $('.allLoadData').fadeOut('slow', function() {
            $(this).remove();
          })

        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Failed to get list of search pages.');
        }

      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  let searchUrl = makeSearchUrl((pageNo - 1) * 10)
  httpRequest.open('get', serverURL + searchUrl, true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}

//pagination 화면
PagingSearch = function(totalCnt, dataSize, pageSize, pageNo) {
  totalCnt = parseInt(totalCnt); // 전체레코드수
  dataSize = parseInt(dataSize); // 페이지당 보여줄 데이타수
  pageSize = parseInt(pageSize); // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
  pageNo = parseInt(pageNo); // 현재페이지
  var html = new Array();
  if (totalCnt == 0) {
    return '';
  }
  // 페이지 카운트
  var pageCnt = totalCnt % dataSize;
  if (pageCnt == 0) {
    pageCnt = parseInt(totalCnt / dataSize);
  } else {
    pageCnt = parseInt(totalCnt / dataSize) + 1;
  }
  var pRCnt = parseInt(pageNo / pageSize);
  if (pageNo % pageSize == 0) {
    pRCnt = parseInt(pageNo / pageSize) - 1;
  }
  //이전 화살표
  if (pageNo > pageSize) {
    var s2;
    if (pageNo % pageSize == 0) {
      s2 = pageNo - pageSize;
    } else {
      s2 = pageNo - pageNo % pageSize;
    }
    html.push('<a class="prevBtn" href=javascript:getSearchList(' + s2 + ')>');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push("</a>");
  } else {
    html.push('<a class="prevBtn" href="#">\n');
    html.push('<img src="../images/arrow_left.svg" alt="prev">');
    html.push('</a>');
  }
  //paging Bar
  for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1; index++) {
    if (index == pageNo) {
      html.push('<strong>');
      html.push(index);
      html.push('</strong>');

    } else {
      html.push('<a href=javascript:getSearchList(' + index + ')>');
      html.push(index);
      html.push('</a>');
    }
    if (index == pageCnt) {
      break;
    }
  }
  //다음 화살표
  if (pageCnt > (pRCnt + 1) * pageSize) {
    var nextPageNum = (pRCnt + 1) * pageSize + 1;
    html.push('<a class="nextBtn" href=javascript:getSearchList(' + nextPageNum + ')>');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  } else {
    html.push('<a class="nextBtn" href="#">');
    html.push('<img src="../images/arrow_right.svg" alt="next">');
    html.push('</a>');
  }
  return html.join('');
}
