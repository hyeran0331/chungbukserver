// Query String 값을 parsing 한다.
var queryString = getQueryStringObject();
// 비밀번호 정규식
var passwordRegExp = RegExp(/^(?=.*[A-Za-z])(?=.*\d)[\w!@#$%^&*()+={}]{8,}$/);
var mailRegExp = RegExp(/^[A-Za-z0-9_\.\-]+@[A-Za-z0-9\-]+\.[A-Za-z0-9\-]+/);
var nameRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var departmentRegExp = RegExp(/^[a-zA-Z.-_ㄱ-ㅎ가-힣\s]+$/);
var spaceRegExp = RegExp(/^[\s]+$/);

// document가 준비되면 명령어를 실행한다.
$(document).ready(function() {

  // 전체 데이터 로딩 화면 없애기
  $('.allLoadData').fadeOut(function() {
    $(this).remove();
  });

  //form 유효성 체크
  $('#email').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#name').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#password').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#con_password').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#department').on('propertychange keyup focus change input keydown', function() {
    checkValidation()
  })
  $('#duty').on('click change focus', function() {
    checkValidation()
  })


})

// 버튼 disabled 부여
function updateDisable() {
  $('.add_btn').removeClass('on');
  $('.add_btn').attr('disabled', true);
}

// 메일 컨펌
function confirmEmail() {
  updateDisable()
  // validation check
  if ($('#email').val() == '') {
    $('.message').find('span').text('* Please enter user email.');
    return false;
  } else if (!mailRegExp.test($('#email').val())) {
    $('.message').find('span').text('* Please enter it according to the email format.');
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 비밀번호 컨펌
function confirmPassword() {
  updateDisable()
  // validation check
  if (!passwordRegExp.test($('#password').val())) {
    if ($('#password').val() == '') {
      $('.message').find('span').text('* Please enter user password');
    } else {
      $('.message').find('span').text('* Please enter at least 8 characters combining English and numbers.');
    }
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}
// 이중 비밀번호 컨펌
function confirmConPassword() {
  updateDisable()
  // validation check
  if ($('#password').val() != $('#con_password').val()) {
    $('.message').find('span').text('* Passwords do not match.');
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 이름 컨펌
function confirmName() {
  updateDisable()
  // validation check
  if ($('#name').val() == '' || spaceRegExp.test($('#name').val()) == true) {
    $('.message').find('span').text('* Please enter a name.');
  } else if (!nameRegExp.test($('#name').val())) {
    $('.message').find('span').text("* Hangul and English or Please enter using '. _ -'.");
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 소속부서 컨펌
function confirmDepartment() {
  updateDisable()
  // validation check
  if ($('#department').val() == '' || spaceRegExp.test($('#department').val()) == true) {
    $('.message').find('span').text('* Please enter a department.');
  } else if (!departmentRegExp.test($('#department').val())) {
    $('.message').find('span').text("* Hangul and English or Please enter using '. _ -'.");
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 권한 컨펌
function confirmAdmin() {
  updateDisable();
  // validation check
  if ($('#duty').val() == 0 || $('#duty').val() == null) {
    $('.message').find('span').text("* Please select the member's duty.");
    return false;
  } else {
    $('.message').find('span').text('');
    return true;
  }
}

// 유효성 체크
function checkValidation() {
  if (!confirmEmail()) {
    return;
  }
  if (!confirmPassword()) {
    return;
  }
  if (!confirmConPassword()) {
    return;
  }
  if (!confirmName()) {
    return;
  }
  if (!confirmDepartment()) {
    return;
  }
  if (!confirmAdmin()) {
    return;
  }
  // 유효성 검사를 통과하면 사용자를 추가한다.
  $('.add_btn').addClass('on');
  $('.add_btn').attr('disabled', false);
}

// 사용자를 추가
function addUser() {
  // httpRequest를 요청한다.
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText addUser= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          linkLastPage()
        } else {
          adminFalseOfDataSuccess(data.reasonCode, data.reason)

        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('post', serverURL + '/admin/api/user/', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({
    'email': $('#email').val(),
    'password': $('#password').val(),
    'name': $('#name').val(),
    'department': $('#department').val(),
    'duty': $('#duty').val()
  }));
}

// 목록 페이지 수 확인
function linkLastPage() {
  var httpRequest = getXMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4) {
      if (httpRequest.status == 200) {
        //서버 응답 결과에 따라 알맞은 작업 처리
        // console.log("httpRequest.responseText linkLastPage= " + httpRequest.responseText)
        var data = JSON.parse(httpRequest.responseText);
        if (data.success == true) {
          var page = data.results.count;
          var totalCnt = parseInt(page); // 전체레코드수
          var dataSize = 5; // 페이지당 보여줄 데이타수
          var pageSize = 10; // 페이지 그룹 범위 1 2 3 5 6 7 8 9 10
          var lastPageNum; // 현재페이지

          if (totalCnt == 0) {
            return '';
          }
          // 페이지 카운트
          var pageCnt = totalCnt % dataSize;
          if (pageCnt == 0) {
            lastPageNum = parseInt(totalCnt / dataSize);
          } else {
            lastPageNum = parseInt(totalCnt / dataSize) + 1;
          }

          // 사용자 리스트의 마지막 pagination으로 이동
          document.location.href = 'adminUserList.html?selectVal=' + queryString.selectVal + '&pageNo=' + lastPageNum
        } else {
          adminFalseOfDataSuccess(data.reasonCode, 'Link transfer failed.')
        }
      } else {
        console.log('code:' + httpRequest.status);
      }
    }
  }
  httpRequest.open('get', serverURL + '/admin/api/user/?email&name&department&duty&offset=0&limit', true);
  httpRequest.withCredentials = true;
  httpRequest.setRequestHeader('Content-Type', 'application/json');
  httpRequest.setRequestHeader('User-Token', getUserToken());
  httpRequest.send(JSON.stringify({}));
}
