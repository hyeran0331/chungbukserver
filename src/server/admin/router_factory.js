/**
 * Admin Router Fetch 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var transfer_log_router = require('./router/transfer_log_router')(app);
  app.use('/admin/api/transferLog', transfer_log_router);

  var user_admin_router = require('./router/user_admin_router')(app);
  app.use('/admin/api/user', user_admin_router);

  var auth_admin_router = require('./router/auth_admin_router')(app);
  app.use('/admin/api/auth', auth_admin_router);

  var popup_router = require('./router/popup_router')(app);
  app.use('/admin/api/popup', popup_router);

  var banner_router = require('./router/banner_router')(app);
  app.use('/admin/api/banner', banner_router);

  var qna_router = require('./router/qna_router')(app);
  app.use('/admin/api/qna', qna_router);

  var type_router = require('./router/type_router')(app);
  app.use('/admin/api/type', type_router);

};
