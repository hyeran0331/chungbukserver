/**
 * Popup Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const popupDao = require('../../dao/admin/popup_dao')

/**
 *  추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await popupDao.insert(
        req.body.type,
        req.body.title,
        req.body.target,
        req.body.link,
        req.body.popupStartDate,
        req.body.popupEndDate,
        req.body.displayLeft,
        req.body.displayTop,
        req.body.size,
        req.body.popupFile,
        req.body.popupContent,
        req.body.exposure
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Popup Add Error.", err)
  }
}

/**
 *  업데이트 함수.
 * @param req
 * @param res
 */
exports.update = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await popupDao.update(
        req.params.id,
        req.body.type,
        req.body.title,
        req.body.target,
        req.body.link,
        req.body.popupStartDate,
        req.body.popupEndDate,
        req.body.displayLeft,
        req.body.displayTop,
        req.body.size,
        req.body.popupFile,
        req.body.popupContent,
        req.body.exposure
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Popup Update Error.", err)
  }
}

/**
 *  삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await popupDao.delete(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Popup Delete Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await popupDao.selectList(req.query.content, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Popup Getting List Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getPopup = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await popupDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Popup Getting Error.", err)
  }
}
