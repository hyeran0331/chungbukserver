/**
 * type Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const typeDao = require('../../dao/admin/type_dao')

/**
 *  추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await typeDao.insert(
        req.body.title,
        req.body.description,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Type Add Error.", err)
  }
}

/**
 *  업데이트 함수.
 * @param req
 * @param res
 */
exports.update = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await typeDao.update(
        req.params.id,
        req.body.title,
        req.body.description,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Type Update Error.", err)
  }
}

/**
 *  삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await typeDao.delete(req.params.id, req.session.authId)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Type Delete Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await typeDao.selectList(req.query.content, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Type Getting List Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getBoardType = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await typeDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Type Getting Error.", err)
  }
}
