/**
 * banner Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const bannerDao = require('../../dao/admin/banner_dao')

/**
 *  추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await bannerDao.insert(
        req.body.position,
        req.body.title,
        req.body.bannerContent,
        req.body.target,
        req.body.link,
        req.body.bannerStartDate,
        req.body.bannerEndDate,
        req.body.exposure,
        req.body.turn,
        req.body.bannerFile,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Banner Add Error.", err)
  }
}

/**
 *  업데이트 함수.
 * @param req
 * @param res
 */
exports.update = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await bannerDao.update(
        req.params.id,
        req.body.position,
        req.body.title,
        req.body.bannerContent,
        req.body.target,
        req.body.link,
        req.body.bannerStartDate,
        req.body.bannerEndDate,
        req.body.exposure,
        req.body.turn,
        req.body.bannerFile,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Banner Update Error.", err)
  }
}

/**
 *  삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await bannerDao.delete(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Banner Delete Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await bannerDao.selectList(req.query.content, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Banner Getting List Error.", err)
  }
}

/**
 *  조회 함수.
 * @param req
 * @param res
 */
exports.getBanner = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await bannerDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Banner Getting Error.", err)
  }
}
