/**
 * Authentication Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const crypto = require('crypto')
const qs = require('querystring');
const userAdminDao = require('../../dao/admin/user_admin_dao')

/**
 * 회원가입 하는 함수.
 * @param req
 * @param res
 */
exports.addUser = function(req, res) {
  if (utils.checkAdminSession(req, res) && utils.checkAdminDuty(req, res)) {
    postUser(req, res, req.body.email, req.body.password)
  }
}

/**
 * 회원 정보 업데이트 함수.
 * @param req
 * @param res
 */
exports.updateUser = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res) && utils.checkAdminDuty(req, res)) {
      var results = await userAdminDao.updateUser(req.params.id, req.body.name, req.body.department, req.body.duty, req.session.authAdminId)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Profile Update Error.", err)
  }
}

/**
  * Password 변경하는 함수.
  * @param req
  * @param res
  */
exports.changePassword = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res) && utils.checkAdminDuty(req, res)) {
      let hashPassword = crypto.createHash("sha512").update(req.body.password).digest("base64");
      let results = await userAdminDao.updatePassword(req.params.id, hashPassword)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * 회원 삭제 함수.
 * @param req
 * @param res
 */
exports.deleteUser = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res) && utils.checkAdminDuty(req, res)) {
      var selectOneResults = await userAdminDao.selectUserById(req.params.id)
      if(selectOneResults.success){
        if(selectOneResults.data.duty == "ADMIN" && selectOneResults.data.id != req.session.authAdminId){
          routerUtils.sendFailWithReason(res, "You can not delete admin user.", err)
        }
        else{
          let results = await userAdminDao.deleteUser(req.params.id, req.session.authAdminId)
          if (results.success) {
            routerUtils.sendFlagAndResult(res, results.success, results.data)
          } else {
            routerUtils.sendFailWithReason(res, results.reason, results.error)
          }
        }
      }
      else{
          routerUtils.sendFailWithReason(res, "There is no deletable user.", err)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Delete Error.", err)
  }
}

/**
 * 회원 정보 조회 함수.
 * @param req
 * @param res
 */
exports.getUserList = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res)) {
      var results = await userAdminDao.selectUserList(req.query.email, req.query.name, req.query.department, req.query.duty, req.query.offset, req.query.limit)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Getting Error.", err)
  }
}

/**
 * 회원 정보 조회 함수.
 * @param req
 * @param res
 */
exports.getUser = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res)) {
      var results = await userAdminDao.selectUserById(req.params.id)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Getting Error.", err)
  }
}

/**
 * 회원 정보 조회를 Email로 하는 함수.
 * @param req
 * @param res
 */
exports.getUserByEmail = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res)) {
      var results = await userAdminDao.selectUserByEmail(req.params.email)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Getting Error.", err)
  }
}

/**
 * Dispenser 회원가입 함수.
 * @param req
 * @param res
 * @param email
 * @param password
 */
async function postUser(req, res, email, password) {
  try {
    var results = await userAdminDao.selectUserByEmail(email)
    if (results.success) {
      var obj = new Object();
      obj.success = false;
      obj.reason = "Eamil is allready used.";
      res.json(obj);
    } else {
      if (results.error == null) {
        let hashPassword = crypto.createHash("sha512").update(password).digest("base64");
        results = await userAdminDao.insertUser(email, hashPassword, req.body.name, req.body.department, req.body.duty)
        if (results.success) {
          routerUtils.sendFlagAndResult(res, results.success, results.data)
        } else {
          routerUtils.sendFailWithReason(res, results.reason, results.error)
        }
      } else {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      }

    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}
