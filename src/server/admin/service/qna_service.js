/**
 * QnA Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const qnaDao = require('../../dao/admin/qna_dao')

/**
 * Qusetion 추가 하는 함수.
 * @param req
 * @param res
 */
exports.addQuestion = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await qnaDao.insertQuestion(
        req.body.title,
        req.body.state,
        req.body.question,
        req.session.authId
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "QnA Add Error.", err)
  }
}

/**
 * Question 업데이트 함수.
 * @param req
 * @param res
 */
exports.updateQuestion = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await qnaDao.updateQuestion(
        req.params.id,
        req.body.title,
        req.body.state,
        req.body.question,
        req.session.authId
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Question Update Error.", err)
  }
}

/**
 * Answer 업데이트 함수.
 * @param req
 * @param res
 */
exports.updateAnswer = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await qnaDao.updateAnswer(
        req.params.id,
        req.body.title,
        req.body.state,
        req.body.answer,
        req.session.authId
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Answer Update Error.", err)
  }
}

/**
 * QnA 삭제 함수.
 * @param req
 * @param res
 */
exports.deleteByUser = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await qnaDao.deleteByUser(req.params.id, "Delete", req.session.authId)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "QnA Delete by user Error.", err)
  }
}

/**
 * QnA 삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await qnaDao.delete(req.params.id, req.session.authId)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "QnA Delete Error.", err)
  }
}

/**
 * QnA List 조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await qnaDao.selectList(req.query.title, req.query.state, req.query.userName, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "QnA List Getting Error.", err)
  }
}

/**
 * QnA List 조회 함수.
 * @param req
 * @param res
 */
exports.getUserQuestionList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await qnaDao.selectQuestionUserList(req.session.authId, req.query.title, req.query.state, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User QnA List Getting Error.", err)
  }
}

/**
 * QnA 조회 함수.
 * @param req
 * @param res
 */
exports.getQnA = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)){
    var results = await qnaDao.selectById(req.params.id)
    routerUtils.sendResult(res, results)
  }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "QnA Getting Error.", err)
  }
}
