/**
 * Authentication Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const express = require('express');
const session = require('express-session');
const crypto = require('crypto');
const generator = require('generate-password');
const authAdminDao = require('../../dao/admin/auth_admin_dao')
const userAdminDao = require('../../dao/admin/user_admin_dao')

/**
 * Login 하는 함수.
 * @param req
 * @param res
 */
exports.login = async function(req, res) {
  try {
    let hashPassword = crypto.createHash('sha512').update(req.body.password).digest('base64');
    let results = await authAdminDao.findLoginUser(req.body.email, hashPassword)
    if (results.success) {
      req.session.authAdminId = results.data.id;
      req.session.authAdminEmail = results.data.email;
      req.session.authAdminDuty = results.data.duty;
      req.session.save(async function() {
        await authAdminDao.updateLoginAt(results.data.id)
        results = await authAdminDao.findLoginUser(req.body.email, hashPassword)
        var obj = new Object();
        obj.success = true;
        obj.session = req.session.authAdminEmail;
        obj.accessToken = results.data.accessToken;
        obj.results = results.data;
        res.json(obj);
      });
    } else {
      routerUtils.sendFailWithReason(res, "Email and password is not matched.", null)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * Session 정보를 가져오는 함수.
 * @param req
 * @param res
 */
exports.getSession = async function(req, res) {
  if (utils.checkAdminSession(req, res)) {
    var obj = new Object();
    obj.success = true;
    obj.session = req.session.authAdminEmail;
    let results = await userAdminDao.selectUserById(req.session.authAdminId)
    if (results.success) {
      obj.results = results.data;
      res.json(obj);
    } else {
      routerUtils.sendFailWithReason(res, results.reason, results.error)
    }
  }
}

/**
 * Logout 함수.
 * @param req
 * @param res
 */
exports.logout = function(req, res) {
  if (utils.checkAdminSession(req, res)) {
    delete req.session.authAdminId;
    delete req.session.authAdminEmail;
    req.session.save(function() {
      routerUtils.sendFlagAndResult(res, true, null)
    });
  }
}

/**
  * Password 변경하는 함수.
  * @param req
  * @param res
  */
exports.changePassword = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res)) {
      let hashOldPassword = crypto.createHash("sha512").update(req.body.oldPassword).digest("base64");
      let hashPassword = crypto.createHash("sha512").update(req.body.password).digest("base64");
      let results = await authAdminDao.updatePassword(req.session.authAdminId, hashOldPassword, hashPassword)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}
