/**
 * TransferLog Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const transferLogDao = require('../../dao/admin/transfer_log_dao')

/**
 * 통신 로그 추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await transferLogDao.insert(req.body.type, req.body.fromDevice, req.body.toDevice, req.body.header, req.body.param, req.body.body)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "TransferLog Add Error.", err)
  }
}

/**
 * 통신 로그 업데이트 함수.
 * @param req
 * @param res
 */
exports.update = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await transferLogDao.update(req.params.id, req.body.type, req.body.fromDevice, req.body.toDevice, req.body.header, req.body.param, req.body.body)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "TransferLog Update Error.", err)
  }
}

/**
 * 통신 로그 삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await transferLogDao.delete(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "TransferLog Delete Error.", err)
  }
}

/**
 * 통신 로그 조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await transferLogDao.selectList(req.query.content, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "TransferLog Getting Error.", err)
  }
}

/**
 * 통신 로그 조회 함수.
 * @param req
 * @param res
 */
exports.getTransferLog = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res) && utils.checkUserDuty(req, res)) {
      var results = await transferLogDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "TransferLog Getting Error.", err)
  }
}
