/**
 * qna CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var router = express.Router();
  var qnaService = require('../service/qna_service');

  /**
   *  추가 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.post('/', function(req, res) {
    qnaService.addQuestion(req, res)
  });

  /**
   *  수 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.put('/question/:id', function(req, res) {
    qnaService.updateQuestion(req, res)
  });

  /**
   *  수 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.put('/answer/:id', function(req, res) {
    qnaService.updateAnswer(req, res)
  });

  /**
   *  삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.delete('/:id', function(req, res) {
    qnaService.delete(req, res)
  });
  /**
   *  삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.delete('/byUser/:id', function(req, res) {
    qnaService.deleteByUser(req, res)
  });


  /**
   *  조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/', function(req, res) {
    qnaService.getList(req, res)
  });

  /**
   *  조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/userQuestionList', function(req, res) {
    qnaService.getUserQuestionList(req, res)
  });

  /**
   *  조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/:id', function(req, res) {
    qnaService.getQnA(req, res)
  });

  return router; //라우터를 리턴
};
