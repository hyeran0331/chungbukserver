/**
 * TransferLog CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var router = express.Router();
  var transferLogService = require('../service/transfer_log_service');

  /**
   * TransferLog 추가 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.post('/', function(req, res) {
    transferLogService.add(req, res)
  });

  /**
   * TransferLog 수 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.put('/:id', function(req, res) {
    transferLogService.update(req, res)
  });

  /**
   * TransferLog 삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.delete('/:id', function(req, res) {
    transferLogService.delete(req, res)
  });

  /**
   * TransferLog 조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/', function(req, res) {
    transferLogService.getList(req, res)
  });

  /**
   * TransferLog 조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/:id', function(req, res) {
    transferLogService.getTransferLog(req, res)
  });

  return router; //라우터를 리턴
};
