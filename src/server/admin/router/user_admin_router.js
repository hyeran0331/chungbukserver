/**
 * User CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var userRouter = express.Router();
  var userAdminService = require('../service/user_admin_service');


  /**
   * User 정보를 생성 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.post('/', function(req, res) {
    userAdminService.addUser(req, res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.put('/:id', function(req, res) {
    userAdminService.updateUser(req, res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.put('/:id/password', function(req, res) {
    userAdminService.changePassword(req, res)
  });

  /**
   * User 정보 하나를 삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.delete('/:id', function(req, res) {
      userAdminService.deleteUser(req, res)
  });

  /**
   * User 정보 하나를 가져오는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.get('/:id', function(req, res) {
    userAdminService.getUser(req, res)
  });

  /**
   * User 정보 목록을 검색해오는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.get('/', function(req, res) {
    userAdminService.getUserList(req, res)
  });

  return userRouter; //라우터를 리턴
};
