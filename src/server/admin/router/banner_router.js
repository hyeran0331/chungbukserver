/**
 * banner CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var router = express.Router();
  var bannerService = require('../service/banner_service');

  /**
   *  추가 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.post('/', function(req, res) {
    bannerService.add(req, res)
  });

  /**
   *  수 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.put('/:id', function(req, res) {
    bannerService.update(req, res)
  });

  /**
   *  삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.delete('/:id', function(req, res) {
    bannerService.delete(req, res)
  });

  /**
   *  조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/', function(req, res) {
    bannerService.getList(req, res)
  });

  /**
   *  조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/:id', function(req, res) {
    bannerService.getBanner(req, res)
  });

  return router; //라우터를 리턴
};
