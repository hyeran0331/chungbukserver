
/**
 * Login 클래스 입니다.
 * @class
 */

module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var authRouter = express.Router();
  var authAdminService = require('../service/auth_admin_service');

  /**
   * 로그인 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.post('/', function(req, res) {
    console.log('req.body = ' + JSON.stringify(req.body));
    authAdminService.login(req,res)
  });

  /**
   * 로그인을 체크 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.get('/', function(req, res) {
    authAdminService.getSession(req,res)
  });

  /**
   * 로그아웃 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.delete('/', function(req, res) {
    authAdminService.logout(req,res)
  });

  /**
   * Password Update 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.put('/', function(req, res) {
    authAdminService.changePassword(req,res)
  });

  return authRouter; //라우터를 리턴
};
