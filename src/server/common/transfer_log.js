module.exports = function(req, res, next) {
  const transferLogDao = require('../dao/admin/transfer_log_dao')
  const [oldWrite, oldEnd] = [res.write, res.end];
  const reqUrl = req.url
  var body = ''


  function clone(obj) {
    if (obj === null || typeof(obj) !== 'object')
      return obj;

    var copy = obj.constructor();

    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        copy[attr] = obj[attr];
      }
    }
    return copy;
  }

  if (reqUrl.includes('/api/')) {
    console.log(req.headers['user-token'])
    console.log('req ' + new Date() + '--------------------------------------------------');
    console.log('req.url = ' + JSON.stringify(req.url));
    console.log('req.session = ' + JSON.stringify(req.session.authEmail));
    console.log('req.params = ' + JSON.stringify(req.params));

    if (reqUrl.includes('/api/auth/')) {
      // password 관련 항목은 로그에서 가려준다.
      var copyBody = clone(req.body)
      if (copyBody.password != null) {
        copyBody.password = "blinded"
      }
      if (copyBody.oldPassword != null) {
        copyBody.oldPassword = "blinded"
      }
      if (copyBody.token != null) {
        copyBody.token = "blinded"
      }
      console.log('req.body = ' + JSON.stringify(copyBody));
      body = JSON.stringify(copyBody)
    } else {
      console.log('req.body = ' + JSON.stringify(req.body));
      body = JSON.stringify(req.body)
    }
    console.log('');
    var user = req.session.authEmail == null ? '' : req.session.authEmail
    if (req.method == 'POST' && res.statusCode == '200') {
      if (reqUrl.includes('/admin/')) {
        loginType = "ADMIN";
      } else {
        loginType = "LOGIN";
      }
    }else{
      loginType = "";
    }
    transferLogDao.insert('REQUEST', loginType, user, req.method + ' ' + decodeURI(reqUrl), JSON.stringify(req.headers), JSON.stringify(req.params), body)
  }

  res.end = function(body) {
    if (reqUrl.includes('/api/')) {
      console.log('res ' + new Date() + '--------------------------------------------------');
      console.log('res.url = ' + JSON.stringify(req.url));
      console.log('res.session = ' + JSON.stringify(req.session.authEmail));
      console.log('res.statusCode = ' + JSON.stringify(res.statusCode));
      console.log('res.body = ' + body);
      console.log('res.headers = ' + JSON.stringify(req.headers));
      console.log('');

      var user = req.session.authEmail == null ? '' : req.session.authEmail
      var adminUser = req.session.authAdminId == null ? '' : req.session.authAdminId

      if (req.method == 'POST' && res.statusCode == '200') {
        if (reqUrl.includes('/admin/')) {
          loginType = "ADMIN";
        } else {
          loginType = "LOGIN";
        }
      }else{
        loginType = "";
      }
      transferLogDao.insert('RESPONSE', loginType, user + '(' + adminUser + ')', req.method + ' ' + decodeURI(reqUrl), JSON.stringify(res.statusCode), JSON.stringify(req.session), JSON.stringify(req.body).substring(0, 100000))
    }
    oldEnd.apply(res, arguments);
  };
  next();
}
