function sendResult(response, result){
  var obj = new Object();
  obj.success = result.success;
  if(obj.success){
    obj.results = result.data;
  }
  else{
    obj.reason = result.reason;
    obj.error = result.error;
  }
  response.json(obj);
}

function sendFlagAndResult(response, success, result){
  var obj = new Object();
  obj.success = success;
  if(result){
    obj.results = result;
  }
  response.json(obj);
}

function sendFailWithReason(response, reason, error){
  var obj = new Object();
  obj.success = false;
  if(reason){
    obj.reason = reason;
  }
  if(error){
    obj.error = error;
  }
  response.json(obj);
}

exports.sendResult = sendResult;
exports.sendFlagAndResult = sendFlagAndResult;
exports.sendFailWithReason = sendFailWithReason;
