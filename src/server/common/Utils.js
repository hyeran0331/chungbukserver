function yyyymmdd(date) {
    var x = date;
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    (d.length == 1) && (d = '0' + d);
    (m.length == 1) && (m = '0' + m);
    var yyyymmdd = y + m + d;
    return yyyymmdd;
}

function checkSession(req, res) {
  var obj = new Object();
  console.log('req.session = ' + JSON.stringify(req.session))
  if(!req.session.authId){
    obj.success = false;
    obj.reasonCode = 1;
    obj.reason = "session out.";
    res.json(obj);
    return false;
  }
  return true;
}

function checkAdminSession(req, res) {
  var obj = new Object();
  console.log('=====================================================')
  console.log('req.session.authAdminId = ' + req.session.authAdminId)
  if(!req.session.authAdminId){
    obj.success = false;
    obj.reasonCode = 1;
    obj.reason = "session out.";
    res.json(obj);
    return false;
  }
  // else if(req.session.authAdminEmail != req.headers['user-token']){
  //   console.log('----------------- email error --------------')
  //   obj.success = false;
  //   obj.reasonCode = 2;
  //   obj.reason = "The user token not matched.";
  //   res.json(obj);
  //   return false;
  // }
  return true;
}

function checkAdminDuty(req, res) {
  var obj = new Object();
  if(req.session.authAdminDuty != 'ADMIN'){
    obj.success = false;
    obj.reasonCode = 3;
    obj.reason = "You can not handle this function.";
    res.json(obj);
    return false;
  }
  return true;
}

function checkAdminUserSession(req, res) {
  var obj = new Object();
  console.log('req.session.authAdminId = ' + req.session.authAdminId)
  console.log('req.session.authId = ' + req.session.authId)
  console.log('req.session.authDuty = ' + req.session.authDuty)
  if(!req.session.authAdminId && !req.session.authId){
    obj.success = false;
    obj.reasonCode = 1;
    obj.reason = "session out.";
    res.json(obj);
    return false;
  }
  return true;
}


function checkUserSession(req, res) {
  var obj = new Object();
  console.log('req.session.authId = ' + req.session.authId)
  if(!req.session.authId){
    obj.success = false;
    obj.reasonCode = 1;
    obj.reason = "session out.";
    res.json(obj);
    return false;
  }
  return true;
}

function checkUserDuty(req, res) {
  console.log('req.session.authDuty = ' + req.session.authDuty)
  var obj = new Object();
  if(req.session.authDuty != 'ADMIN'){
    obj.success = false;
    obj.reasonCode = 3;
    obj.reason = "You can not handle this function.";
    res.json(obj);
    return false;
  }
  return true;
}

exports.yyyymmdd = yyyymmdd;
exports.checkSession = checkSession;
exports.checkAdminSession = checkAdminSession;
exports.checkAdminDuty = checkAdminDuty;
exports.checkAdminUserSession = checkAdminUserSession;
exports.checkUserDuty = checkUserDuty;
exports.checkUserSession = checkUserSession;
