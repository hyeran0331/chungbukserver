/**
 * User Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const crypto = require('crypto')
const userDao = require('../../dao/user/user_dao')
// const userAdminDao = require('../../dao/admin/user_admin_dao')
const nodemailer = require('nodemailer')
const qs = require('querystring');
const mailService = require('./mail_service');

/**
 * 회원가입 하는 함수.
 * @param req
 * @param res
 */
 exports.addUser = async function(req, res) {
   try {
     var results = await userDao.selectUserByEmail(req.body.email, req.body.backupEmail)
     if (results.success) {
       var obj = new Object();
       obj.success = false;
       obj.reason = "Email is allready used.";
       res.json(obj);
     } else {
       if (results.error == null) {
         let hashPassword = crypto.createHash("sha512").update(req.body.password).digest("base64");
         results = await userDao.insertUser(
           req.body.email,
           req.body.backupEmail,
           hashPassword,
           req.body.userName,
           req.body.gender,
           req.body.phoneNumber,
           req.body.generalNumber,
           req.body.job,
           req.body.area,
           req.body.affiliation,
           req.body.major,
           req.body.position,
           req.body.comTelephone,
           req.body.comFax,
           req.body.postalCode,
           req.body.address,
           req.body.detailAddress,
           req.body.grade,
           req.body.duty,
         )
         if (results.success) {
           sendConfirmMail(req, res, results);
         } else {
           routerUtils.sendFlagAndResult(res, results)
         }
       } else {
         routerUtils.sendFlagAndResult(res, results)
       }
     }
   } catch (err) {
     console.log(err);
     routerUtils.sendFailWithReason(res, "User Profile Add Error.", err)
   }
 }


/**
 * 회원 정보 업데이트 함수.
 * @param req
 * @param res
 */
exports.updateUser = async function(req, res) {
  try {
    if (utils.checkSession(req, res)) {
      var results = await userDao.selectUserByEmail(req.body.email, req.body.backupEmail)
      if (results.success) {
        var obj = new Object();
        obj.success = false;
        obj.reason = "Email is allready used.";
        res.json(obj);
      } else {
        var results = await userDao.updateUser(
          req.params.id,
          req.body.email,
          req.body.backupEmail,
          req.body.userName,
          req.body.engFirstName,
          req.body.engLastName,
          req.body.phoneNumber,
          req.body.generalNumber,
          req.body.job,
          req.body.area,
          req.body.affiliation,
          req.body.major,
          req.body.position,
          req.body.comTelephone,
          req.body.comFax,
          req.body.postalCode,
          req.body.address,
          req.body.detailAddress,
          req.body.memo,
          req.body.grade,
          req.body.duty,
        )
        if (results.success) {
          req.session.authId = req.params.id;
          req.session.authEmail = req.body.email;
          req.session.authUserName = req.body.userName;
          req.session.save(async function() {
            // await userAuthDao.updateLoginAt(results.data.id)
          });
        }
      }
      routerUtils.sendFlagAndResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Profile Update Error.", err)
  }
}

/**
 * Password 변경하는 함수.
 * @param req
 * @param res
 */
exports.changePassword = async function(req, res) {
  try {
    if (utils.checkSession(req, res)) {
      let hashPassword = crypto.createHash("sha512").update(req.body.password).digest("base64");
      let results = await userDao.updatePassword(req.session.authId, hashPassword)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * Grade 변경하는 함수.
 * @param req
 * @param res
 */
exports.changeUserInfo = async function(req, res) {
  try {
    if (utils.checkAdminUserSession(req, res)) {
      let results = await userDao.updateUserInfo(req.params.id, req.body.grade, req.body.duty, req.body.memo)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * 회원 삭제 함수.
 * @param req
 * @param res
 */
exports.deleteUser = async function(req, res) {
  try {
    if (utils.checkAdminSession(req, res)) {
      var selectOneResults = await userDao.selectUserById(req.params.id)
      if (selectOneResults.success) {
        // if (selectOneResults.data.id != req.session.authId) {
        //   routerUtils.sendFailWithReason(res, "You can not delete.", null)
        // } else {
        //   let results = await userDao.deleteUser(req.params.id, selectOneResults.data.password)
        //   routerUtils.sendFlagAndResult(res, results)
        // }
          let results = await userDao.deleteUser(req.params.id, selectOneResults.data.password)
          routerUtils.sendFlagAndResult(res, results)
      } else {
        routerUtils.sendFailWithReason(res, "There is no deletable user.", null)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Delete Error.", err)
  }
}

/**
 * 회원 정보 조회 함수.
 * @param req
 * @param res
 */
exports.getUserList = async function(req, res) {
  try {
    if (utils.checkAdminUserSession(req, res)) {
      var results = await userDao.selectUserList(
        req.query.content,
        req.query.userName,
        req.query.phoneNumber,
        req.query.email,
        req.query.affiliation,
        req.query.grade,
        req.query.created_at,
        req.query.offset,
        req.query.limit)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Getting Error.", err)
  }
}

/**
 * 회원 정보 조회 함수.
 * @param req
 * @param res
 */
exports.getUser = async function(req, res) {
  try {
    if (utils.checkAdminUserSession(req, res)) {
      var results = await userDao.selectUserById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Getting Error.", err)
  }
}

/**
 * 회원 정보 조회 함수.
 * @param req
 * @param res
 */
exports.getConfirmUser = async function(req, res) {
  try {
    var results = await userDao.updateConfirm(req.params.id, req.query.code)
    routerUtils.sendFlagAndResult(res, results)
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "User Confirm Error.", err)
  }
}


// // 수정
function sendConfirmMail(req, res, results) {
  mailService.sendEmail(req, res, results.data.email,
    "[CONNECT] Request User Confirm.",
    'if you want to join, please click this link. \n http://localhost:8083/api/user/confirm/' + results.data.id + "?code=" + qs.escape(results.data.backupEmail))
}
