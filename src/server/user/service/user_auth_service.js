/**
 * Authentication Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const express = require('express');
const session = require('express-session');
const crypto = require('crypto');
const generator = require('generate-password');
const userAuthDao = require('../../dao/user/user_auth_dao');
const userDao = require('../../dao/user/user_dao');
const mailService = require('./mail_service')


/**
 * Login 하는 함수.
 * @param req
 * @param res
 */
exports.login = async function(req, res) {

  try {
    let hashPassword = crypto.createHash('sha512').update(req.body.password).digest('base64');
    let results = await userAuthDao.findLoginUser(req.body.email, hashPassword)
    if (results.success) {
      req.session.authId = results.data.id;
      req.session.authEmail = results.data.email;
      req.session.authUserName = results.data.userName;
      req.session.authDuty = results.data.duty;
      req.session.save(async function() {
        await userAuthDao.updateLoginAt(results.data.id)
        console.log("results.data.mailConfimedAt = " + results.data.mailConfimedAt);
        if(results.data.mailConfimedAt){
          results = await userAuthDao.findLoginUser(req.body.email, hashPassword)
          var obj = new Object();
          obj.success = true;
          obj.session = req.session.authEmail;
          obj.results = results.data;
          res.json(obj);
        }
        else{
          routerUtils.sendFailWithReason(res, "Account is not confirmed.", null)
        }
      });
    } else {
      routerUtils.sendFailWithReason(res, "Email and password is not matched.", null)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * Session 정보를 가져오는 함수.
 * @param req
 * @param res
 */
exports.getSession = async function(req, res) {
  if (utils.checkSession(req, res)) {
    var obj = new Object();
    obj.success = true;
    obj.session = req.session.authEmail;
    let results = await userDao.selectUserById(req.session.authId)
    if (results.success) {
      obj.results = results.data;
      res.json(obj);
    } else {
      routerUtils.sendFailWithReason(res, results.reason, results.error)
    }
  }
}

/**
 * Logout 함수.
 * @param req
 * @param res
 */
exports.logout = function(req, res) {
  if (utils.checkSession(req, res)) {
    delete req.session.authId;
    delete req.session.authEmail;
    delete req.session.authUserName;
    req.session.save(function() {
      routerUtils.sendFlagAndResult(res, true, null)
    });
  }
}

/**
 * Password 변경하는 함수.
 * @param req
 * @param res
 */
exports.changePassword = async function(req, res) {
  try {
    if (utils.checkSession(req, res)) {
      let hashOldPassword = crypto.createHash("sha512").update(req.body.oldPassword).digest("base64");
      let hashPassword = crypto.createHash("sha512").update(req.body.password).digest("base64");
      let results = await userAuthDao.updatePassword(req.session.authId, hashOldPassword, hashPassword)
      if (results.success) {
        routerUtils.sendFlagAndResult(res, results.success, results.data)
      } else {
        routerUtils.sendFailWithReason(res, results.reason, results.error)
      }
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, null, err)
  }
}

/**
 * Password 새로 생성하여 Mail 보내는 함수 (Forget Password).
 * @param req
 * @param res
 */
 exports.changePasswordAndSendMail = async function(req, res) {
   try {
     let password = generator.generate({length: 6, numbers: true, uppercase: true, symbols:false}) + generator.generate({length: 4, numbers: false, uppercase: false, symbols:true}) + "!";
     let hashPassword = crypto.createHash("sha512").update(password).digest("base64");
     var results = await userAuthDao.updatePasswordByEmail(req.body.backupEmail, hashPassword)
     if(results.success){
       results = await userDao.selectUserByBackupEmail(req.body.backupEmail)
       if(results.success){
          mailService.sendEmail(req, res, req.body.backupEmail, "[CONNECT] change password complate.", 'your email is ' + results.data.email +'\nyour password is ' + password)
       }
       else{
         routerUtils.sendFlagAndResult(res, results.success, results.data)
       }
     }
     else{
       routerUtils.sendFlagAndResult(res, results.success, results.data)
     }
   } catch (err) {
     console.log(err);
     routerUtils.sendFailWithReason(res, "Password Update Error", JSON.stringify(err))
   }
 }
