/**
 * Mail Service 클래스 입니다.
 * @class
 */
const nodemailer = require('nodemailer');
const routerUtils = require('../../common/router_utils');

/**
 * 메일을 전송한다.
 * @param req
 * @param res
 * @param toAddress
 * @param subject
 * @param content
 * @param html
 */
exports.sendEmail = function(req, res, toAddress, subject, content, html) {
  var smtpTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com', port: 465, secure: true, // use SSL
    auth: {
      user: 'web@holich.net',
      pass: 'cozyholich!!'
    }
  });

  var mailOptions = {
    from: '홀리츠웹 <web@holich.net>',
    to: toAddress,
    subject: subject,
    text: content,
    html: html
  };

  smtpTransport.sendMail(mailOptions, function(error, response) {
    console.log("finish email");
    if (error) {
      console.log(error);
      routerUtils.sendFailWithReason(res, "Send Email Error", error)

    } else {
      console.log("Message sent : " + response.message);
      routerUtils.sendFlagAndResult(res, true, null)
    }
    smtpTransport.close();
  });
}
