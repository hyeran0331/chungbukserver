/**
 * Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const boardDao = require('../../dao/user/board_dao')

/**
 * 추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.insert(
        req.body.typeId,
        req.body.title,
        req.body.content,
        req.session.authId,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Add Error.", err)
  }
}

/**
 * 추가 하는 함수.
 * @param req
 * @param res
 */
exports.addComment = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.insertComment(
        req.body.typeId,
        req.body.title,
        req.body.content,
        req.body.boardId,
        req.body.parentId,
        req.session.authId,
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Add Error.", err)
  }
}

/**
 * 업데이트 함수.
 * @param req
 * @param res
 */
exports.update = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.update(
        req.params.id,
        req.body.typeId,
        req.body.title,
        req.body.content,
        req.session.authId
      )
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Update Error.", err)
  }
}

/**
 * 삭제 함수.
 * @param req
 * @param res
 */
exports.delete = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.delete(req.params.id, req.session.authId)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Delete Error.", err)
  }
}

/**
 * List 조회 함수.
 * @param req
 * @param res
 */
exports.getList = async function(req, res) {
  console.log("### getList");
  console.log("req.query = " + JSON.stringify(req.query));
  
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.selectList(req.query.type, req.query.typeId, req.query.title, req.query.content, req.query.offset, req.query.limit)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board List Getting Error.", err)
  }
}


/**
 * 조회 함수.
 * @param req
 * @param res
 */
exports.getBoard = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await boardDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "Board Getting Error.", err)
  }
}
