/**
 * Service 클래스 입니다.
 * @class
 */

const utils = require('../../common/Utils');
const routerUtils = require('../../common/router_utils');
const express = require('express');
const session = require('express-session');
const fileDao = require('../../dao/user/file_dao')

/**
 * 추가 하는 함수.
 * @param req
 * @param res
 */
exports.add = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      if(req.files.freeBoard != undefined){
        var results = await fileDao.insert(
          req.files.freeBoard[0].fieldname,
          req.files.freeBoard[0].originalname,
          req.files.freeBoard[0].encoding,
          req.files.freeBoard[0].mimetype,
          req.files.freeBoard[0].destination,
          req.files.freeBoard[0].filename,
          req.files.freeBoard[0].path,
          req.files.freeBoard[0].size,
          req.body.postId
        )
      }else if(req.files.inquiryBoard != undefined){
        var results = await fileDao.insert(
          req.files.inquiryBoard[0].fieldname,
          req.files.inquiryBoard[0].originalname,
          req.files.inquiryBoard[0].encoding,
          req.files.inquiryBoard[0].mimetype,
          req.files.inquiryBoard[0].destination,
          req.files.inquiryBoard[0].filename,
          req.files.inquiryBoard[0].path,
          req.files.inquiryBoard[0].size,
          req.body.postId
        )
      }
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "file Add Error.", err)
  }
}

/**
 * 조회 함수.
 * @param req
 * @param res
 */
exports.get = async function(req, res) {
  try {
    if (utils.checkUserSession(req, res)) {
      var results = await fileDao.selectById(req.params.id)
      routerUtils.sendResult(res, results)
    }
  } catch (err) {
    console.log(err);
    routerUtils.sendFailWithReason(res, "file Getting Error.", err)
  }
}
