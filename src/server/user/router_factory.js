/**
 * User Router Fetch 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음
  var user_router = require('./router/user_router')(app);
  app.use('/api/user', user_router);

  var auth_router = require('./router/auth_router')(app);
  app.use('/api/auth', auth_router);

  var board_router = require('./router/board_router')(app);
  app.use('/api/board', board_router);

  var file_router = require('./router/file_router')(app);
  app.use('/api/file', file_router);

};
