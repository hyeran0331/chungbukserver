/**
 *CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var router = express.Router();
  var boardService = require('../service/board_service');

  /**
   * 추가 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.post('/', function(req, res) {
    boardService.add(req, res)
  });
  /**
   * 추가 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.post('/comments', function(req, res) {
    boardService.addComment(req, res)
  });

  /**
   * 수 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.put('/:id', function(req, res) {
    boardService.update(req, res)
  });

  /**
   * 삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.delete('/:id', function(req, res) {
    boardService.delete(req, res)
  });

  /**
   * 조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/', function(req, res) {
    boardService.getList(req, res)
  });

  /**
   * 조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/:id', function(req, res) {
    boardService.getBoard(req, res)
  });

  return router; //라우터를 리턴
};
