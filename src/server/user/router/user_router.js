/**
 * User CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음
  var express = require('express');
  var userRouter = express.Router();
  var userService = require('../service/user_service');

  /**
   * User 정보를 생성 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.post('/', function(req, res) {
    userService.addUser(req, res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.put('/:id', function(req, res) {
    userService.updateUser(req, res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.put('/:id/password', function(req, res) {
    userService.changePassword(req, res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.put('/:id/changeUserInfo', function(req, res) {
    userService.changeUserInfo(req, res)
  });

  /**
   * User 정보 하나를 삭제 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.delete('/:id', function(req, res) {
    userService.deleteUser(req, res)
  });

  /**
   * User 정보 하나를 가져오는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.get('/:id', function(req, res) {
    userService.getUser(req, res)
  });

  /**
   * User 정보 목록을 검색해오는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.get('/', function(req, res) {
    userService.getUserList(req, res)
  });

  /**
   * User Confirm 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  userRouter.get('/confirm/:id', function(req, res) {
    userService.getConfirmUser(req, res)
  });

  return userRouter; //라우터를 리턴
};
