/**
 * CRUD 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음

  var express = require('express');
  var router = express.Router();
  var multer = require('multer');
  var path = require('path');
  var fs = require('fs')
  var fileService = require('../service/file_service');

  var storage = multer.diskStorage({
    destination: function(req, file, cb) {
      // let boardId = req
      console.log('file.fieldname = ' +file.fieldname);
      var uploadFieldName = file.fieldname
      cb(null, 'src/server/views_main/uploads/'+uploadFieldName) // cb 콜백함수를 통해 전송된 파일 저장 디렉토리 설정
    },
    filename: function(req, file, callback) {  // 저장될 파일
      let array = file.originalname.split('.');
      array[0] = array[0] + '_';
      array[1] = '.' + array[1];
      array.splice(1, 0, Date.now().toString());
      const result = array.join('');
      callback(null, result);
    }

  })

  const upload = multer({
    storage,
    limits: { //업로드 파일 제한
      files: 10,  //업로드 파일 개수제한
      fileSize: 1024 * 1024 * 1024, //업로드 파일 사이즈제한 1GB
    }
  });

  router.post('/', upload.fields([{ name: 'freeBoard' }, { name: 'inquiryBoard' }]), function(req, res) {
    // console.log(req.files.freeBoard); // 콘솔(터미널)을 통해서 req.file Object 내용 확인 가능.
    // console.log(req.files.inquiryBoard);
    fileService.add(req, res)
  });

  /**
   * 조회 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  router.get('/:id', function(req, res) {
    fileService.get(req, res)
  });

  return router; //라우터를 리턴
};
