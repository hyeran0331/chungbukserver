/**
 * Login 클래스 입니다.
 * @class
 */
module.exports = function(app) { //함수로 만들어 객체 app을 전달받음
  var express = require('express');
  var authRouter = express.Router();
  var userAuthService = require('../service/user_auth_service');

  /**
   * 로그인 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.post('/', function(req, res) {
      userAuthService.login(req,res)
  });

  /**
   * User 정보를 수정 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.put('/', function(req, res) {
    userAuthService.changePassword(req,res)
  });

  /**
   * 세선사용자 정보를 가져오는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.get('/', function(req, res) {
    userAuthService.getSession(req,res)
  });

  /**
   * 로그아웃 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.delete('/', function(req, res) {
    userAuthService.logout(req,res)
  });

  /**
   * Password 분실 시 호출 하는 함수.
   * @param path {string} URL 문자열.
   * @param function {myCallback} 호출될 함수.
   */
  authRouter.post('/forget', function(req, res) {
    userAuthService.changePasswordAndSendMail(req,res)
  });

  return authRouter; //라우터를 리턴
};
