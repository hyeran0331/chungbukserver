const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin')

const outputDirectory = 'server_dist';


module.exports = {
  optimization: {
    // We no not want to minimize our code.
    minimize: false
  },
  externals: ['pg', 'sqlite3', 'tedious', 'pg-hstore'],
  entry: [
    'babel-polyfill', './src/server/index.js'
  ],
  output: {
    path: path.join(__dirname, outputDirectory),
    filename: 'server_bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader'
      }
    }, {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    }, {
      test: /\.(png|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000'
    }]
  },
  target: 'node',
  devServer: {
    disableHostCheck: true,
    historyApiFallback: true,
    host: "0.0.0.0",
    port: 8082,
    open: true,
    proxy: {
      '/api': 'http://localhost:8083'
    }
  },
  plugins: [
    // new TerserPlugin({
    //   parallel: true,
    //   terserOptions: {
    //     ecma: 6,
    //   },
    // }),
    new CleanWebpackPlugin([outputDirectory]),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './public/favicon.ico'
    })
  ]
};
